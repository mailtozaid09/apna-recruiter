import React from 'react'
import { colors } from '../../global/colors'
import { Text, View, ActivityIndicator, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native'
import { screenHeight, screenWidth } from '../../global/constants'
import { media } from '../../global/media'
import { Inter } from '../../global/fontFamily'

const Input = ({title, value, placeholder, defaultValue, onChangeText, icon, onPress}) => {
    return (
        <View style={styles.container} >
            {title && <Text style={{fontSize: 16, marginBottom: 8, color: colors.black, fontFamily: Inter.Medium,}}>{title}</Text>}
            <View style={styles.inputContainer} >
                <TextInput
                    placeholder={placeholder} 
                    value={value}
                    onChangeText={onChangeText}
                    placeholderTextColor="#ACACAC"
                    style={[styles.input, {marginRight: icon ? -20 : null}]}
                    defaultValue={defaultValue}
                />

                {icon && <TouchableOpacity onPress={onPress} >
                    <Image source={media.send} style={{height: 24, width: 24, resizeMode: 'contain', marginRight: 20}} />
                </TouchableOpacity>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        marginBottom: 10,
        width: '100%',
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center', 
        justifyContent: 'space-between', 
        paddingHorizontal: 15,
        width: '100%',
        borderRadius: 8,
        borderWidth: 1,
        backgroundColor: '#F4F6F7',
        borderColor: '#B3BAC5'
    },
    input: {
        height: 50,
        fontSize: 18,
        fontFamily: Inter.Light,
        width: '100%',
        color: colors.black,
    }
})

export default Input