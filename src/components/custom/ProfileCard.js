import React, {useState, useEffect} from 'react'
import {StyleSheet, Text, View, Image} from 'react-native';
import SwipeCards from 'react-native-swipe-cards';

import { screenWidth } from '../../global/constants';
import { Inter } from '../../global/fontFamily';
import { media } from '../../global/media';
import { profileCards } from '../../global/sampleData';
import { colors } from '../../global/colors';



const ProfileCard = (props) => {
   

    const { name, title, location, workTitle_1, workSubtitle_1, workDuration_1, workTitle_2, workSubtitle_2, workDuration_2 , workTitle_3, workSubtitle_3, workDuration_3  } = props;

    return(
        <View style={styles.card}>
            <View style={styles.profileTop} >
                <Image style={styles.thumbnail} source={media.dummy_user} />
                {/* <View style={styles.thumbnail} /> */}
                <Text style={styles.title}>{name}</Text> 
                <Text style={styles.subTitle}>{title}</Text> 
                <Text style={styles.description}>{location}</Text> 
            </View>

            <View style={{height: 0.5, width: '100%', backgroundColor: colors.grey}} />

            <View style={styles.profileBottom} >
     
                    <View style={{flexDirection: 'row', paddingVertical: 10, paddingBottom: 14, alignItems: 'flex-start', width: '100%', borderBottomWidth: 1, borderColor: '#E9ECEF'}} >
                        <Image source={media.post_job} style={{height: 22, width: 22, marginRight: 15}} />
                        <View style={{flex: 1}} >
                            <Text style={styles.workTitle}>{workTitle_1}</Text> 
                            <Text style={styles.workSubtitle}>{workSubtitle_1}</Text> 
                            <Text style={styles.workDuration}>{workDuration_1}</Text> 
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', paddingVertical: 10, paddingBottom: 14, alignItems: 'flex-start', width: '100%', borderBottomWidth: 1, borderColor: '#E9ECEF'}} >
                        <Image source={media.post_job} style={{height: 22, width: 22, marginRight: 15}} />
                        <View style={{flex: 1}} >
                            <Text style={styles.workTitle}>{workTitle_2}</Text> 
                            <Text style={styles.workSubtitle}>{workSubtitle_2}</Text> 
                            <Text style={styles.workDuration}>{workDuration_2}</Text> 
                        </View>
                    </View>
                    <View style={styles.viewProfile} >
                        <Text style={styles.viewProfileText} >View Profile</Text>
                    </View>
            </View>
            {/* <View style={{height: 0.5, width: '100%', backgroundColor: colors.grey}} /> */}
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center',
        width: screenWidth,
        backgroundColor: colors.white,
    },
    card: {
        width: screenWidth-40,
        alignItems: 'center',
        borderRadius: 6,
        // overflow: 'hidden',
        borderColor: '#fff',
        backgroundColor: 'white',
        borderWidth: 1,
        elevation: 1,
    },
    profileTop: {
        paddingVertical: 10,
        width: '100%', 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
    },
    profileBottom: {
        alignItems: 'center', 
        justifyContent: 'center',
        width: '100%',
        padding: 10,
        paddingTop: 10,
    },
    viewProfile: {
        padding: 10,
        //marginTop: 12,
        width: '100%',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: '#1F8268',
    },
    viewProfileText: {
        fontSize: 16,
        fontFamily: Inter.Medium,
        color: '#1F8268',
    },
    title: {
        fontSize: 20,
        fontFamily: Inter.Bold,
        color: '#172B4D',
        marginBottom: 4,
        marginTop: 10,
    },
    subTitle: {
        fontSize: 14,
        fontFamily: Inter.Medium,
        textAlign: 'center',
        color: '#5E6C84',
        marginBottom: 4
    },
    description: {
        fontSize: 12,
        fontFamily: Inter.Regular,
        color: '#5E6C84',
        marginBottom: 4,
    },
    thumbnail: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 2,
    },
    text: {
      fontSize: 20,
    },
    noMoreCards: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    workTitle: {
        fontSize: 16,
        fontFamily: Inter.Medium,
        color: '#172B4D',
    },
    workSubtitle: {
        fontSize: 14,
        fontFamily: Inter.Medium,
        color: '#172B4D',
    },
    workDuration: {
        fontSize: 12,
        fontFamily: Inter.Medium,
        color: '#5E6C84',
    }
  })


export default ProfileCard