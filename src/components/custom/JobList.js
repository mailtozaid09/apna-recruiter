import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity, Linking, FlatList } from 'react-native'
import { colors } from '../../global/colors'
import { Inter } from '../../global/fontFamily'



const JobList = ({data}) => {

    return (
        <View style={{width: '100%', marginTop: 10, flex: 1,}} >

            

            <FlatList
                data={data}
                style={{}}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{width: '100%', }}
                renderItem={({item, index}) => {
                    return(
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {}} 
                            style={styles.cardContainer}
                            key={index} 
                        >
                            <View style={{padding: 20, paddingTop: 0, paddingBottom: 10, }} >
                                <View style={{flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between'}}>
                                    <View style={{ flex: 1, marginRight: 10 }} >
                                        <Text numberOfLines={1} style={styles.title}>{item?.title}</Text>
                                    </View>
                                    <View style={[styles.approvedContainer, {backgroundColor: '#E9ECEF', }]} >
                                        <Text style={[styles.approvedText, {color: item?.created_by?.company_verification_status != 'non_verified' ? '#1F8268' : '#CC0000' }]}>{item?.created_by?.company_verification_status == 'non_verified' ? 'Not Approved' : 'Approved'}</Text>
                                    </View>
                                </View>
                                <Text style={styles.subTitle}>{item?.organization.name} - {item?.company_address?.area?.name}, {item?.company_address?.area?.city?.name}</Text>
                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                    <Text style={styles.description1}>Posted on: <Text style={styles.description2}> {item?.created_on}</Text></Text>
                                    <Text style={styles.description1}>By: <Text style={styles.description2}> {item?.created_by?.full_name}</Text></Text>
                                </View>
                                <Text style={styles.description1}>For: <Text style={styles.description2}> Apnatime Tech</Text></Text>
                            </View>


                            <View style={styles.applicationContainer} >
                                <View>
                                    <Text style={styles.appTitle} >0</Text>
                                    <Text style={styles.appSubTitle}>Total Review</Text>
                                </View>

                                <View style={{height: 30, width: 1, marginHorizontal: 25, backgroundColor: '#B3BAC5'}} />

                                <View>
                                    <Text style={styles.appTitle}>0</Text>
                                    <Text style={styles.appSubTitle}>Total Application</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
        </View> 
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        backgroundColor: colors.white,
        marginBottom: 18,
        //padding: 20,
        paddingBottom: 0,
        paddingVertical: 12,
    },
    title: {
        fontSize: 16,
        fontFamily: Inter.Bold,
        color: '#172B4D',
    },
    subTitle: {
        fontSize: 14,
        marginTop: 6,
        fontFamily: Inter.Regular,
        color: '#5E6C84',
        marginBottom: 4
    },
    description1: {
        fontSize: 12,
        fontFamily: Inter.Medium,
        color: '#172B4D',
        marginBottom: 4,
    },
    description2: {
        fontSize: 12,
        fontFamily: Inter.Regular,
        color: '#172B4D',
        marginBottom: 4,
    },
    alljobs: {
        fontSize: 20,
        fontFamily: Inter.SemiBold,
        color: '#172B4D',
        marginBottom: 10
    },
    applicationContainer: {
        //backgroundColor: '#F4F6F7',
        borderTopWidth: 1,
        borderColor: '#ced4da',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 6,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    appTitle: {
        fontSize: 18,
        fontFamily: Inter.SemiBold,
        color: '#172B4D',
        marginBottom: 0,
    },
    appSubTitle: {
        fontSize: 10,
        fontFamily: Inter.Regular,
        color: '#172B4D',
    },
    approvedText: {
        fontSize: 12,
        fontFamily: Inter.Regular,
        color: '#172B4D',
    },
    approvedContainer: {
        height: 30,
        width: 100,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    postButton: {
        backgroundColor: '#1F8268',
        padding: 14,
        paddingVertical: 8,
        borderRadius: 8,
    },
    postButtonText:{
        fontSize: 14,
        color: colors.white,
        fontFamily: Inter.Regular,
    }
})

export default JobList