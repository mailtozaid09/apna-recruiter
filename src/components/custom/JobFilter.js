import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity, Linking, FlatList } from 'react-native'
import { colors } from '../../global/colors'
import { Inter } from '../../global/fontFamily'
import { media } from '../../global/media'

const data = [
    {
        id: 1,
        title: 'Icon',
    },
    {
        id: 2,
        title: 'All',
    },
    {
        id: 3,
        title: 'Expired',
    },
    {
        id: 4,
        title: 'Select Plan',
    },
    {
        id: 5,
        title: 'Not Applied',
    },
]

const JobFilter = ({}) => {

    const [activeTab, setActiveTab] = useState('All');
    return (
        <View style={{width: '100%', borderTopWidth: 1, borderColor: '#ced4da', }} >

            <FlatList
                data={data}
                horizontal
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{backgroundColor: '#F2F2F0', padding: 20, paddingVertical: 15, paddingBottom: 5}}
                renderItem={({item, index}) => {
                    return(
                        <TouchableOpacity
                            activeOpacity={0.5}
                            onPress={() => {setActiveTab(item.title)}} 
                            style={[styles.cardContainer, {backgroundColor: activeTab == item.title ? '#0062FF10' : '#F4F6F7', borderColor: activeTab == item.title ? '#0062FF' : '#B3BAC5' }]}
                            key={index} 
                        >
                            {item.title == 'Icon' ? 
                            <Image source={media.sort} style={{height: 18, width: 18, }} /> 
                            : 
                            <Text style={[styles.title, {color: activeTab == item.title ? '#0062FF' : '#5E6C84'} ]} >{item.title}</Text>
                            }
                            
                        </TouchableOpacity>
                    )
                }}
            />
        </View> 
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        borderWidth: 1,
        borderRadius: 10,
        //marginBottom: 15,
        padding: 10,
        paddingVertical: 6,
        marginRight: 10,
        minWidth: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 16,
        marginHorizontal: 20,
        fontFamily: Inter.Regular,
    },
  
})

export default JobFilter