import React, {useState, useRef} from 'react'
import RBSheet from 'react-native-raw-bottom-sheet'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, KeyboardAvoidingView, FlatList, Image } from 'react-native'
import { colors } from '../../utils/colors'
import { screenHeight, screenWidth } from '../../utils/constants'

import Lottie from 'lottie-react-native';
import { Inter } from '../../global/fontFamily'
import LoginButton from '../button'
import { useNavigation } from '@react-navigation/native'


const SuccessSheet = ({successRefRBSheet, editRefRBSheet, onCloseSuccessSheet, onCloseHireSheet, onOpenHireSheet, onCloseEditSheet, onOpenEditSheet}) => {
    const scrollViewRef = useRef();

    const navigation = useNavigation()

    const question = [
        {
            "heading": "Basic Details",
            "name": "Zaid Ahmed",
            "city": "Bangalore",
        },
        {
            "heading": "Qualification",
            "name": "Zaid Ahmed",
            "city": "Bangalore",
        },
        {
            "heading": "Experience",
            "name": "FrontEnd Developer",
            "ccity": "Bangalore",
        },
    ]


    const [initialQuestion, setInitialQuestion] = useState(question);

    const [inputValue, setInputValue] = useState('');
    const [trigger, setTrigger] = useState(1);

    const [loader, setLoader] = useState(false);

    return (
        <View>
            <RBSheet
                ref={successRefRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                onClose={() => {
                  
                }}
                height={450}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090"
                    },
                    draggableIcon: {
                        backgroundColor: "#000",
                        width: 80
                    },
                    container: {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20
                    }
                }}
            >
                <View style={{justifyContent: 'space-between', width: screenWidth, flex: 1}} >
                    <View style={{alignItems: 'center',}} >
                        <Lottie
                            autoPlay loop
                            style={{height: screenWidth-100, width: screenWidth-100}}
                            source={require('../../assets/121018-done.json')} 
                        />

                        <Text style={{fontSize: 18, paddingHorizontal: 40, textAlign: 'center', fontFamily: Inter.SemiBold}} >Your job has been successfully posted</Text>
                    </View>

                    <View style={{marginHorizontal: 20, marginBottom: 30}} >
                        <LoginButton
                            title="VIEW JOB"
                            onPress={() => {
                                onCloseSuccessSheet();
                                navigation.navigate('JobsScreen')
                            }}
                        />
                    </View>

                </View>
                
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    headerTitle: {
        fontSize: 22,
        color: colors.black,
        fontWeight: 'bold'
    },
    headerSubTitle: {
        fontSize: 16,
        color: colors.gray,
        fontWeight: 'bold'
    },
    botMessages: {
        borderRadius: 6,
        marginBottom: 10,
        padding: 14,
        backgroundColor: '#f8f9fa'
    },
    userMessages: {
        borderRadius: 6,
        marginBottom: 10,
        padding: 14,
        backgroundColor: '#D2F4FF'
    },
    loginButton: {
        height: 50,
        borderRadius: 4,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: '#1F8268',
    },
    loginButtonText: {
        fontSize: 16,
        color: colors.white,
        fontFamily: Inter.SemiBold,
    },
})

   

export default SuccessSheet