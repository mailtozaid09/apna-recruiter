import React, {useState, useRef, useEffect} from 'react'
import RBSheet from 'react-native-raw-bottom-sheet'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, KeyboardAvoidingView, FlatList, Image, TextInput } from 'react-native'

import { screenHeight, screenWidth } from '../../utils/constants'
import Input from '../input'
import { media } from '../../global/media'

import EditIcon from 'react-native-vector-icons/MaterialCommunityIcons'

import DownIcon from 'react-native-vector-icons/Entypo'

import AutoTypingText from 'react-native-auto-typing-text';

import Lottie from 'lottie-react-native';
import { Inter } from '../../global/fontFamily'
import { colors } from '../../global/colors'


const defaultOptions = [
    {
    id: 1,
    title: 'Post a Smart Job',
    image: media.post_job_green,
    color: '#E9F8F4',
    type: 'defaultOptions',
    textColor: '#20B88F'
},
{
    id: 2,
    title: 'Search Candidates using AI',
    image: media.search_db,
    color: '#E3EEFF',
    type: 'defaultOptions',
    textColor: '#005DF2'
},
{
    id: 3,
    title: 'Other Options',
    image: media.search_icon,
    color: '#FEF3D9',
    textColor: '#FFBC25'
},
]

const jobDetails = {
    "Job Details": {
        "Name": "Zaid Ahmed",
        "Job Title": "Salesforce Developer",
        "Company": "Koch Buisness Solutions",
        "City": "Bangalore",
    },
    "Qualification": {
        "Degree": "BTech IT",
        "Year": "2023",
        "College": "Christ University",
        "D.O.B": "09/02/2000",
    },
    "Experience": {
        "Role": "Frontend Developer",
        "Exp.": "2 years",
    },
}


const SmartHireSheet = ({refRBSheet, onCloseHireSheet, onCloseSuccessSheet, onOpenSuccessSheet, onOpenEditSheet}) => {
    const scrollViewRef = useRef();


    const question = []


    const [initialQuestion, setInitialQuestion] = useState(question);

    const [inputValue, setInputValue] = useState('');
    const [trigger, setTrigger] = useState(1);

    const [loader, setLoader] = useState(false);


    const postSmartJobTrigger = (value) => {
       
        if(value.triggerStep == 1){

            var newArray = [
                {
                    user: "user",
                    message: [
                        value.title
                    ],
                },
            ]
            setInitialQuestion(initialQuestion.concat(newArray));
    
            setLoader(true)
    

            var botArray = [
                {
                    user: "bot",
                    message: [
                        "Great! Let's post a job in less than 30 seconds. Select from one of the sample jobs or type in the chat below."
                    ],
                    options: [
                        {
                            trigger: 1,
                            triggerStep: 2,
                            type: 'options',
                            title: "Post a job in Bangalore graphic designer salary 50000 experience 2 years",
                        },
                        {
                            trigger: 1,
                            triggerStep: 2,
                            type: 'options',
                            title: "Post a job in Gurgaon Ui designer salary 50000 experience 2 years",
                        },
                        {
                            trigger: 1,
                            triggerStep: 2,
                            type: 'options',
                            title: "Post a job in Delhi Visual designer salary 50000 experience 2 years",
                        },
                    ],
                },
            ]

            var array = newArray.concat(botArray)

            setTimeout(() => {
                setLoader(false)
                setInitialQuestion(initialQuestion.concat(array));
            }, 2500);
           
        }else if(value.triggerStep == 2){

            

            var msgObject = {
                user: "user",
                message: [
                    value.title
                ],
            }
    

            setInitialQuestion(prevArray => [...prevArray, msgObject])
            setLoader(true)


            
            var msgObject1 = {
                user: "bot",
                message: [
                    "Your job preview is ready"
                ],
            }
    
            setTimeout(() => {
                setInitialQuestion(prevArray => [...prevArray, msgObject1])
                setLoader(false)
            }, 1000);


            var msgObject2 = {
                user: "bot",
                    jobPreview: [
                        {
                            "heading": "Job Details",
                            "name": "Zaid Ahmed",
                            "city": "Bangalore",
                        },
                        {
                            "heading": "Qualification",
                            "name": "Zaid Ahmed",
                            "city": "Bangalore",
                        },
                        {
                            "heading": "Experience",
                            "name": "FrontEnd Developer",
                            "ccity": "Bangalore",
                        },
                    ],
            }
    
            setTimeout(() => {
                setInitialQuestion(prevArray => [...prevArray, msgObject2])
                setLoader(false)
            }, 5000);





            var msgObject3 = {
                user: "bot",
                message: [
                    "Please confirm to post the job"
                ],
            }
    
            setTimeout(() => {
                setInitialQuestion(prevArray => [...prevArray, msgObject3])
                setLoader(false)
            }, 8000);



            var msgObject4 = {
                user: "bot",
                jobPlans: [
                    {
                        trigger: 1,
                        triggerStep: 4,
                        type: 'options',
                        plan: 'Basic',
                        title: "Post job using basic plan",
                    },
                    {
                        trigger: 1,
                        triggerStep: 4,
                        type: 'options',
                        plan: 'Premium',
                        title: "Post job using premium plan",
                    },
                ],
            }
    
            setTimeout(() => {
                setInitialQuestion(prevArray => [...prevArray, msgObject4])
                setLoader(false)
            }, 12000);


            // var newArray = [
            //     {
            //         user: "user",
            //         message: [
            //             value.title
            //         ],
            //     },
            //     {
            //         user: "bot",
            //         message: [
            //             "Your job preview is ready"
            //         ],
            //     }
                
            // ]

            // setInitialQuestion(initialQuestion.concat(newArray));


            // setLoader(true)

            var botArray = [
                {
                    user: "bot",
                    jobPreview: [
                        {
                            "heading": "Job Details",
                            "name": "Zaid Ahmed",
                            "city": "Bangalore",
                        },
                        {
                            "heading": "Qualification",
                            "name": "Zaid Ahmed",
                            "city": "Bangalore",
                        },
                        {
                            "heading": "Experience",
                            "name": "FrontEnd Developer",
                            "ccity": "Bangalore",
                        },
                    ],
                   //jobPostButton: "Post the job ->",
                },
            ]

            // var array = newArray.concat(botArray)
       
            // setTimeout(() => {
            //     setLoader(false)
            //     setInitialQuestion(initialQuestion.concat(array));
            // }, 4500);
            


            // var botArray2 = [
            //     {
            //         user: "bot",
            //         message: [
            //             "Please confirm to post the job"
            //         ],
                    
            //     },
            // ]

            // var array2 = array.concat(botArray2)

            // setTimeout(() => {
            //     setLoader(true)
            //     setInitialQuestion(initialQuestion.concat(array2));
            // }, 7000);

          

            // var botArray3 = [
            //     {
            //         user: "bot",
            //         options: [
            //             {
            //                 trigger: 1,
            //                 triggerStep: 4,
            //                 type: 'options',
            //                 title: "Post job using basic plan",
            //             },
            //             {
            //                 trigger: 1,
            //                 triggerStep: 4,
            //                 type: 'options',
            //                 title: "Post job using premium plan",
            //             },
            //         ],
            //     },
            // ]
    
            

            // var array3 = array2.concat(botArray3)
    
            // setTimeout(() => {
            //     setLoader(false)
            //     setInitialQuestion(initialQuestion.concat(array3));
            // }, 12000);



        
       
        }else if(value.triggerStep == 3){
       
            var newArray = [
                {
                    user: "user",
                    message: [
                        value.title
                    ],
                },
                {
                    user: "bot",
                    message: [
                        "Your job preview is ready"
                    ],
                    jobPreview: [
                        {
                            "heading": "Job Details",
                            "name": "Zaid Ahmed",
                            "city": "Bangalore",
                        },
                        {
                            "heading": "Qualification",
                            "name": "Zaid Ahmed",
                            "city": "Bangalore",
                        },
                        {
                            "heading": "Experience",
                            "name": "FrontEnd Developer",
                            "ccity": "Bangalore",
                        },
                    ],
                   //jobPostButton: "Post the job ->",
                },
            ]

            setLoader(true)

        setTimeout(() => {
            setLoader(false)
            setInitialQuestion(initialQuestion.concat(newArray));
        }, 2500);
        }else if(value.triggerStep == 4){
            setLoader(true)
           
            var newArray = {
                user: "user",
                message: [
                    value.title
                ],
            }
    
            setTimeout(() => {
                setInitialQuestion(prevArray => [...prevArray, newArray])
                setLoader(false)
            }, 2500);

            setTimeout(() => {
                postJobResponse()
            }, 3000);
            
            
            // var newArray = [
            //     {
            //         user: "user",
            //         message: [
            //             value.title
            //         ],
            //     },
            // ]

            // setLoader(true)

            // setTimeout(() => {
            //     setLoader(false)
            //     setInitialQuestion(initialQuestion.concat(newArray));
        
            // }, 2500);


            
        }
    }


    const jobOpeningsTrigger = () => {
    }


    const otherOptionsTrigger = () => {
    }

    const defaultOptionsFunc = (value) => {

        var msgObject = {
            user: "user",
            message: [
                value.title
            ],
        }

        setInitialQuestion(prevArray => [...prevArray, msgObject])
        setLoader(true)


        var msgObject1 = {
            user: "bot",
            message: [
                "Great! Let's post a job in less than 30 seconds. Select from one of the sample jobs or type in the chat below."
            ],
        }

        setTimeout(() => {
            setInitialQuestion(prevArray => [...prevArray, msgObject1])
            setLoader(false)
        }, 1000);


        var msgObject2 =  {
            user: "bot",
            jobOptions: [
                {
                    trigger: 1,
                    triggerStep: 2,
                    type: 'options',
                    jobTitle: 'Graphic Designer',
                    location: 'Bangalore',
                    salary: '50000',
                    experience: '2 years',
                    title: "Graphic Designer job in Bangalore with salary 50000 experience 2 years",
                },
                {
                    trigger: 1,
                    triggerStep: 2,
                    type: 'options',
                    jobTitle: 'Software Developer',
                    location: 'Gurgaon',
                    salary: '60000',
                    experience: '3 years',
                    title: "Software Developer job in Gurgaon with salary 60000 experience 3 years",
                },
            ],
        }

        setTimeout(() => {
            setInitialQuestion(prevArray => [...prevArray, msgObject2])
            setLoader(false)
        }, 10000);

        // var newArray = [
        //     {
        //         user: "user",
        //         message: [
        //             value.title
        //         ],
        //     },
        //     {
        //         user: "bot",
        //         message: [
        //             "Great! Let's post a job in less than 30 seconds. Select from one of the sample jobs or type in the chat below."
        //         ],
        //     }
        // ]
        // setInitialQuestion(initialQuestion.concat(newArray));

        // setLoader(true)


        // var botArray = [

        //     {
        //         user: "bot",
        //         options: [
        //             {
        //                 trigger: 1,
        //                 triggerStep: 2,
        //                 type: 'options',
        //                 title: "Post a job in bangalore graphic designer salary 50000 experience 2 years",
        //             },
        //             {
        //                 trigger: 1,
        //                 triggerStep: 2,
        //                 type: 'options',
        //                 title: "Post a job in Gurgaon Ui designer salary 50000 experience 2 years",
        //             },
        //         ],
        //     },
        // ]

        // var array = newArray.concat(botArray)
        // setTimeout(() => {
        //     setLoader(false)
        //     setInitialQuestion(initialQuestion.concat(array));
        // }, 10000);
    }

    const inputOption = (value) => {
        setInputValue('')

        var newArray = [
            {
                user: "user",
                message: [
                    value
                ],
            },
            {
                user: "bot",
                message: [
                    "Oh-no!",
                ],
            }
        ]

        setLoader(true)

        setTimeout(() => {
            setLoader(false)
            setInitialQuestion(initialQuestion.concat(newArray));
        }, 2500);
    }

    const updateResponse = (option, type) => {
        
       
      
        if(type == 'input'){
            inputOption(option)
        }else if(option?.type == 'defaultOptions'){
            defaultOptionsFunc(option)
        }else if(option?.type == 'options'){
            if(option?.trigger == 1){
                postSmartJobTrigger(option)
            }else if(option?.trigger == 2){
                jobOpeningsTrigger(option)
            }else if(option?.trigger == 3){
                otherOptionsTrigger(option)
            }    
        }

    }



    const postJobResponse = () => {
        var newArray = [
            
            {
                user: "bot",
                message: [
                    "Job posted succesfully!",
                ],
            }
        ]

        setLoader(true)

        setTimeout(() => {
            setLoader(false)
            setInitialQuestion(initialQuestion.concat(newArray));
            
            onCloseHireSheet()
            onOpenSuccessSheet()
        }, 2500);
    }

    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={screenHeight - 80}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090"
                    },
                    draggableIcon: {
                        backgroundColor: "#000",
                        width: 80
                    },
                    container: {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20
                    }
                }}
            >
                 <KeyboardAvoidingView behavior='position' contentContainerStyle={{ height: '100%'}} >

                 <View style={{flexDirection: 'row', borderBottomWidth: 1, borderColor: colors.light_gray, paddingBottom: 4, padding: 10, paddingLeft: 20, paddingTop: 0}} >
                        <Text style={styles.headerTitle} >Welcome to </Text>
                        <Image source={media.apnaHire} style={{height: 40, width: 100,  marginTop: -3.5,  resizeMode: 'contain'}} />
                        <Text style={styles.headerTitle} >AI</Text>
                    </View>

                    

                <ScrollView 
                     ref={scrollViewRef}
                     onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}

                    contentContainerStyle={{marginBottom: 100, alignItems: 'flex-start', }} style={{marginBottom: 100}} >
                    <View style={{ width: screenWidth}} >
                    
                        <View style={{padding: 20, paddingBottom: 0}} >
                            
                            <Text style={styles.headerSubTitle} >Your personal hiring co-pilot</Text>


                            <View style={{flexDirection: 'row', width: '100%', justifyContent: 'flex-start', alignItems: 'flex-end',  marginVertical: 25, }} >
                                <Text style={[styles.headerSubTitle, { color: '#172B4D', marginRight: 14}]} >Choose an option below</Text>
                                <View style={{height: 1, flex: 1, marginBottom: 4, backgroundColor: colors.light_gray}} />
                            </View>


                            {defaultOptions?.map((item, index) => (
                                <TouchableOpacity 
                                    key={index}
                                    onPress={() => {updateResponse(item, 'defaultOptions')}}
                                    style={{width: '100%', paddingHorizontal: 12, flexDirection: 'row', backgroundColor: item.color, height: 50, marginBottom: 12, borderRadius: 10, alignItems: 'center', justifyContent: 'space-between'}} >
                                   
                                        <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                            <Image source={item.image} style={{height: 24, width: 24, marginRight: 20}} />
                                            <Text style={{fontSize: 16, color: item.textColor, fontFamily: Inter.SemiBold,}} >{item.title}</Text>
                                        </View>

                                        {item.title == 'Other Options' && <DownIcon name="chevron-down" color={item.textColor} size={24} style={{}} />}
                                </TouchableOpacity>
                            ))}
                        </View> 
                        

                        <View style={{marginBottom: 20}} >
                            {initialQuestion?.map((item, index) => (
                                <View key={index} style={item.user == 'bot' ? {alignItems: 'flex-start', } : {alignItems: 'flex-end', width: '100%', marginBottom: 15,}} >
                                    <View style={{paddingHorizontal: 20}} >
                                        {item?.message && item?.message?.length != 0 && (
                                            <View style={[item.user == 'bot' ? styles.botMessages : styles.userMessages]} >
                                                {item?.message?.map((msg, idx) => (
                                                
                                                <View key={idx} style={{ alignItems: 'flex-start', width: '100%', }} >
                                                {item.user == 'user' 
                                                    ? 
                                                    <Text style={{fontSize: 14, marginRight: 15, fontFamily: Inter.Medium, color: item.user == 'bot' ? '#172B4D'  :  '#fff'}} >{msg}</Text>
                                                    :
                                                    <AutoTypingText
                                                        text={msg}
                                                        charMovingTime={60}
                                                        delay={0}
                                                        style={{
                                                            width: '100%',
                                                            color: '#172B4D',
                                                            backgroundColor: 'rgba(0,0,0,0)',
                                                        }}
                                                        onComplete={() => { console.log('done'); }}
                                                        />
                                                    }
                                                </View>
                                            ))}
                                        
                                            {item.user == 'bot' ?
                                                <Image source={media.left_rect} style={{height: 25, width: 25, resizeMode: 'contain', position: 'absolute', left: -17, top: -4}} />
                                                :
                                                <Image source={media.ticks} style={{height: 18, width: 18, resizeMode: 'contain', position: 'absolute', right: 8, bottom: 8}} />
                                            }
                                            </View>
                                        )}
                                    </View>


                                    {item?.options && <View style={{backgroundColor: '#F1F3F2', marginTop: 10, paddingVertical: 10, width: '100%'}} >
                                        {item?.options?.length != 0 && 
                                            <View style={{ width: '100%' , padding: 6}} >
                                                {item?.options?.map((option, idx) => (
                                                    <TouchableOpacity
                                                        key={idx} 
                                                        style={styles.optionsCard} 
                                                        onPress={() => {updateResponse(option, 'options')}}
                                                    >
                                                            <View style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EAEEEC', marginRight: 14}} >
                                                                <Image source={media.post_job_green} style={{height: 22, width: 22}} />
                                                            </View>
                                                            <View style={{flex: 1, marginRight: 10}}>
                                                                <Text style={{fontSize: 12, fontFamily: Inter.Regular, color: item.user == 'bot' ? '#5E6C84'  :  colors.white}} >{option?.title}</Text>
                                                            </View>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>}
                                    </View>}

                                    {item?.jobPlans && <View style={{backgroundColor: '#F1F3F2', marginTop: 10, paddingVertical: 10, width: '100%'}} >
                                        {item?.jobPlans?.length != 0 && 
                                            <View style={{ width: '100%' , padding: 6}} >
                                                {item?.jobPlans?.map((option, idx) => (
                                                    <TouchableOpacity
                                                        key={idx} 
                                                        style={styles.optionsCard} 
                                                        onPress={() => {updateResponse(option, 'options')}}
                                                    >
                                                            <View style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EAEEEC', marginRight: 14}} >
                                                                <Image source={media.post_job_green} style={{height: 22, width: 22}} />
                                                            </View>
                                                            <View style={{flex: 1, marginRight: 10}}>
                                                                <Text style={{fontSize: 12, fontFamily: Inter.Regular, color: item.user == 'bot' ? '#5E6C84'  :  colors.white}} >Post job using <Text style={{fontFamily: Inter.Bold}} >{option?.plan}</Text> plan</Text>
                                                            </View>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>}
                                    </View>}


                                    {item?.jobOptions && <View style={{backgroundColor: '#F1F3F2', marginTop: 10, paddingVertical: 10, width: '100%'}} >
                                        {item?.jobOptions?.length != 0 && 
                                            <View style={{ width: '100%' , padding: 6}} >
                                                {item?.jobOptions?.map((option, idx) => (
                                                    <TouchableOpacity
                                                        key={idx} 
                                                        style={styles.optionsCard} 
                                                        onPress={() => {updateResponse(option, 'options')}}
                                                    >
                                                            <View style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EAEEEC', marginRight: 14}} >
                                                                <Image source={media.post_job_green} style={{height: 22, width: 22}} />
                                                            </View>
                                                            <View style={{flex: 1, marginRight: 10}}>
                                                               <Text style={{fontSize: 12, fontFamily: Inter.Regular, color: item.user == 'bot' ? '#5E6C84'  :  colors.white}} >
                                                                    <Text style={{fontFamily: Inter.Bold}} >{option?.jobTitle} </Text> 
                                                                    job in 
                                                                    <Text style={{fontFamily: Inter.Bold}} > {option?.location} </Text>
                                                                    with salary 
                                                                    <Text style={{fontFamily: Inter.Bold}} > {option?.salary} </Text> 
                                                                    and experience 
                                                                    <Text style={{fontFamily: Inter.Bold}} > {option?.salary} </Text> 
                                                                </Text>
                                                                {/* <Text style={{fontSize: 12, fontFamily: Inter.Regular, color: item.user == 'bot' ? '#5E6C84'  :  colors.white}} >{option?.title}</Text> */}
                                                            </View>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>}
                                    </View>}


                                    {item?.jobPreview && item?.jobPreview?.length != 0 && 
                                    <View style={{ height: 250, marginBottom: 15, backgroundColor: '#F1F3F2', marginTop: 10, paddingLeft: 20, paddingTop: 10}} >
                                        <ScrollView  horizontal style={{ height: 180}} >
                                            {Object.entries(jobDetails).map((obj)  => {
                                                return(
                                                    <TouchableOpacity
                                                    onPress={() => {
                                                        onCloseHireSheet();
                                                        onOpenEditSheet();
                                                    }}
                                                    style={styles.previewCard}>
                                                        <View style={{flexDirection: 'row', borderBottomWidth: 0.5, borderColor: colors.white, paddingBottom: 4, alignItems: 'center', justifyContent: 'space-between', marginBottom: 10,}} >
                                                            <View style={{flexDirection: 'row', alignItems: 'center'}} > 
                                                                <Image source={
                                                                    obj[0] == 'Job Details' 
                                                                    ? media.post_job : 
                                                                    obj[0] == 'Qualification' 
                                                                    ? media.salary :
                                                                    media.job
                                                                    } 
                                                                    style={{height: 25, width: 25, resizeMode: 'contain', marginRight: 10 }} />
                                                                <Text style={{fontSize: 16, color: '#172B4D', fontFamily: Inter.SemiBold, }} >{obj[0]}</Text> 
                                                            </View>    
                                                            <EditIcon name="pencil" color="#2BB793" size={22} />
                                                            
                                                        </View>

                                                        <View>
                                                            {Object.keys(obj[1]).map((item) =>{
                                                                return(
                                                                    <View style={{flexDirection: 'row', borderColor: '#5E6C84', borderBottomWidth: 0.5, marginBottom: 6, }} >
                                                                        <View style={{width: '40%',}} >
                                                                            <Text style={{fontSize: 14, fontFamily: Inter.SemiBold, color: '#5E6C84', }} >{item}</Text>  
                                                                        </View>
                                                                        <View style={{flex: 1, paddingBottom: 6, }} >
                                                                            <Text style={{fontSize: 14, fontFamily: Inter.Regular, color: '#5E6C84', }} >{obj[1][item]}</Text>  
                                                                        </View>
                                                                    </View>
                                                                )
                                                            })}
                                                        </View>

                                                    </TouchableOpacity>   
                                                )
                                            })}
                                            
                                            </ScrollView>
                                    </View>}

                                    
                                </View>
                            ))}
                        </View>
                    
              

                        <View>
                            {loader
                                    ?
                                    <View style={{ width: '100%', alignItems: 'center'}} >
                                        <View style={{  height: 50, marginTop: 10, alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10, borderRadius: 10}} >
                                            {/* <Text style={{fontSize: 16, color: colors.black}} >Waiting to get reponse . . .</Text> */}
                                            <Lottie 
                                                autoPlay loop
                                                style={{height: 80, width: 100}}
                                                source={require('../../assets/97930-loading.json')} 
                                            />
                                        </View>
                                    </View>
                                    :
                                    null
                                }   
                        </View>
                    
                    </View>
                  

                 

                    
                </ScrollView>

               
                
                <View style={{ position: 'absolute', bottom: 40, width: '100%',  }} >
                    <View style={{alignItems: 'center', flexDirection: 'row', }} >
                        <View style={{ flex: 1}} >
                            <Input
                            value={inputValue}
                            placeholder='Click here to type'
                            icon={media.send}
                            onChangeText={(text) => setInputValue(text)}
                            onPress={() => updateResponse(inputValue, 'input')}
                        />
                        </View>
                        <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', width: 50, height: 50, marginRight: 20, borderRadius: 25, backgroundColor: '#1F8268', marginBottom: 10}} >
                            <Image source={media.microphone} style={{height: 24, width: 24, resizeMode:'contain' }} />
                        </TouchableOpacity>
                    </View> 
                </View>
              
                </KeyboardAvoidingView>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    headerTitle: {
        fontSize: 20,
        color: colors.black,
        fontFamily: Inter.SemiBold,
    },
    headerSubTitle: {
        fontSize: 16,
        color: colors.gray,
        fontFamily: Inter.SemiBold,
    },
    botMessages: {
        borderRadius: 6,
        marginBottom: 5,
        padding: 14,
        marginRight: 10,
        backgroundColor: '#F1F3F2',
        width: '100%'
    },
    userMessages: {
        borderRadius: 6,
        marginTop: 15,
        padding: 14,
        backgroundColor: '#20B88F'
    },
    optionsCard: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginBottom: 6, 
        //width: '100%',
        //height: 40, 
        padding: 10, 
        paddingVertical: 6,
        borderRadius: 50, 
        margin: 4, 
        borderColor: '#B3BAC5', 
        backgroundColor: '#F7F7F7', 
        borderWidth: 1,
        shadowColor: "#000",
        flexDirection: 'row',
        alignItems: 'center',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 10.49,

        elevation: 10,
    },
    
    previewCard: {
        height: 220,
        width: 260,
        borderWidth: 1,
        borderRadius: 10,
        padding: 12,
        marginRight: 12,
        marginTop: 4,
        borderColor: '#B3BAC5', 
        backgroundColor: '#F7F7F7', 
        borderWidth: 0.5,
        elevation: 2,
    }
})


export default SmartHireSheet