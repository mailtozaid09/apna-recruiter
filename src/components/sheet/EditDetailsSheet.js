import React, {useState, useRef} from 'react'
import RBSheet from 'react-native-raw-bottom-sheet'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, KeyboardAvoidingView, FlatList, Image } from 'react-native'
import { colors } from '../../utils/colors'
import { screenHeight, screenWidth } from '../../utils/constants'
import Input from '../input'
import { media } from '../../global/media'
import { Inter } from '../../global/fontFamily'
import LoginButton from '../button'




const EditDetailsSheet = ({editRefRBSheet, onCloseHireSheet, onOpenHireSheet, onCloseEditSheet, onOpenEditSheet}) => {


    return (
        <View>
            <RBSheet
                ref={editRefRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                onClose={() => {
                    onOpenHireSheet()
                }}
                height={screenHeight - 80}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090"
                    },
                    draggableIcon: {
                        backgroundColor: "#000",
                        width: 80
                    },
                    container: {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20
                    }
                }}
            >
                       <View style={{flexDirection: 'row', borderBottomWidth: 1, paddingHorizontal: 20, borderColor: colors.light_gray, paddingBottom: 4, padding: 10, paddingTop: 0}} >
                                <Text style={styles.headerTitle} >Welcome to </Text>
                                <Image source={media.apnaHire} style={{height: 40, width: 100,  marginTop: -3.5, resizeMode: 'contain'}} />
                                <Text style={styles.headerTitle} >AI</Text>
                        </View>

                    <ScrollView>
                        
                        <Text style={{fontSize: 16, fontFamily: Inter.Medium, color: '#5E6C84', paddingLeft: 20, marginVertical: 10 }} >Edit Details</Text>

                        <View>
                         
                            <Input
                                title="Name"
                                placeholder="Your Name"
                                defaultValue="Zaid Ahmed"
                            />

                            <Input
                                title="City"
                                placeholder="Your City"
                                defaultValue="Bangalore"
                            />

                            <Input
                                title="Date of Birth"
                                placeholder="Your date of birth"
                                defaultValue="09/02/2000"
                            />

                            <Input
                                title="Phone Number"
                                placeholder="Enter your phone number"
                                defaultValue="+91 9027346976"
                            />

                        </View>

                      
                </ScrollView>
                <View style={{marginHorizontal: 20, marginBottom: 30}} >
                    <LoginButton
                        title="SAVE DETAILS"
                        onPress={() => {
                            onOpenHireSheet();
                            onCloseEditSheet();
                        }}
                    />
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    headerTitle: {
        fontSize: 20,
        color: colors.black,
        fontFamily: Inter.SemiBold,
    },
    headerSubTitle: {
        fontSize: 16,
        color: colors.gray,
        fontFamily: Inter.SemiBold,
    },
    botMessages: {
        borderRadius: 6,
        marginBottom: 10,
        padding: 14,
        backgroundColor: '#f8f9fa'
    },
    userMessages: {
        borderRadius: 6,
        marginBottom: 10,
        padding: 14,
        backgroundColor: '#D2F4FF'
    },
    loginButton: {
        height: 50,
        borderRadius: 8,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: '#1F8268',
    },
    loginButtonText: {
        fontSize: 16,
        color: colors.white,
        fontFamily: Inter.SemiBold,
    },
})

   

export default EditDetailsSheet