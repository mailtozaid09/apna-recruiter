import React from 'react'

import { Text, View, ActivityIndicator, StyleSheet, Image } from 'react-native'

import { media } from '../../../global/media'
import { colors } from '../../../utils/colors'
import { screenWidth } from '../../../global/constants'

const Header = ({navigation}) => {
    return (
        <View style={styles.container}> 
            <Image source={media.apnaRecruiter} style={{height: 60, width: 150, marginTop: 10, resizeMode: 'contain'}} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: screenWidth-30,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: colors.light_gray
    }
})

export default Header