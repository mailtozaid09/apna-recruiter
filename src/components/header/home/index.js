import React from 'react'

import { Text, View, ActivityIndicator, StyleSheet, Image } from 'react-native'
import { screenWidth } from '../../../global/constants'
import { media } from '../../../global/media'

import BellIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { colors } from '../../../global/colors'

const HomeHeader = ({navigation}) => {
    return (
        <View style={styles.container} >
            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                <Image source={media.menu} style={{height: 28, width: 28, marginRight: 10, resizeMode: 'contain'}} />
                <Image source={media.apnaRecruiter} style={{height: 60, width: 150, resizeMode: 'contain'}} />
            </View>

           <BellIcon name="bell-badge-outline" size={26} color={colors.black}  />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: screenWidth-30,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: colors.light_gray,
        marginTop: 10,
    }
})

export default HomeHeader