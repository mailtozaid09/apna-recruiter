import React from 'react'
import { colors } from '../../global/colors'
import { Text, View, ActivityIndicator, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { screenHeight, screenWidth } from '../../global/constants'
import { media } from '../../global/media'
import { Inter } from '../../global/fontFamily'

const LoginButton = ({title, disabled, onPress, buttonLoader}) => {
    return (
        <View style={styles.container} >
            <TouchableOpacity
                    onPress={onPress}
                    disabled={buttonLoader ? true : false}
                    style={styles.loginButton}
                >
                    {buttonLoader ? 
                        <ActivityIndicator size="large" color={colors.white} />
                    : 
                        <Text style={styles.loginButtonText} >{title}</Text>
                    }
                    
            </TouchableOpacity> 
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // paddingHorizontal: 20,
        // marginBottom: 10,
        width: '100%',
    },
    loginButton: {
        height: 50,
        borderRadius: 8,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: '#1F8268',
    },
    loginButtonText: {
        fontSize: 16,
        color: colors.white,
        fontFamily: Inter.SemiBold,
    },
})

export default LoginButton