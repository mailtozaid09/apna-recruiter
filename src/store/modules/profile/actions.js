import { Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

let nextTodoId = 0;

export function currentProfile(current_profile) {
    return {
        type: '@profile/CURRENT_PROFILE',
        payload: {
            current_profile,
        },
    };
}




export const addUserProfile = params => {
    return {
        type: '@profile/ADD_USER_PROFILE',
        payload: {
            id: ++nextTodoId,
            user_name: params.user_name,
            user_icon: params.user_icon,
        },
    };
};



export const updateUserProfile = params => {

    return {
        type: '@profile/UPDATE_USER_PROFILE',
        payload: {
            id: params.id,
            user_name: params.user_name,
            user_icon: params.user_icon,
        },
    };
};


export const deleteUserProfile = id => {
    return {
        type: '@profile/DELETE_USER_PROFILE',
        payload: {
            id
        },
    };
};
