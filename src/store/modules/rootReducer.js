import { combineReducers } from 'redux';

import auth from './auth/reducer';
import home from './home/reducer';
import profile from './profile/reducer';

export default combineReducers({
  auth,
  home,
  profile
});
