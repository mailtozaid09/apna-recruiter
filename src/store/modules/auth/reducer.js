const INITIAL_STATE = {
  user: null,
  loader: false
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case '@auth/SIGN_IN':
      return {
        ...state,
        user: action.payload.user,
      };
    case '@auth/SIGN_OUT':
      return { ...state, user: null };
    
    //   case '@auth/SCREEN_LOADER':
    //   return {
    //     ...state,
    //     loader: action.payload.loader,
    // };
    default:
      return state;
  }
}
