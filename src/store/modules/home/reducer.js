const INITIAL_STATE = {
  loader: false,
  smart_hire_sheet: false,
  session_token: null,
};

export default function home(state = INITIAL_STATE, action) {
    switch (action.type) {
        case '@home/SMART_HIRE_SHEET':
        return {
            ...state,
            smart_hire_sheet: action.payload.smart_hire_sheet,
        };
       
        
        case '@home/LOADER':
        return {
            ...state,
            loader: action.payload.loader,
        };

        case '@home/SESSION_TOKEN':
        return {
            ...state,
            session_token: action.payload.session_token,
        };

        default:
        return state;
    }
}
