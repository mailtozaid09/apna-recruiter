import { Alert } from 'react-native';
import { users } from '../../../global/sampleData';
import AsyncStorage from '@react-native-async-storage/async-storage';

export function screenLoader(loader) {
    return {
        type: '@home/LOADER',
        payload: {
            loader,
        },
    };
}


export function sessionToken(session_token) {
    return {
        type: '@home/SESSION_TOKEN',
        payload: {
            session_token,
        },
    };
}


export function smartHireBottomSheet(smart_hire_sheet) {
    return {
        type: '@home/SMART_HIRE_SHEET',
        payload: {
            smart_hire_sheet,
        },
    };
}

