import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, ViewBase, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { media } from '../../global/media';
import LoginScreen from '../../screens/auth/login';
import VerificationScreen from '../../screens/auth/login/VerficationScreen';
import Header from '../../components/header/login';



const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Login" 
        >
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={({navigation}) => ({
                    headerShown: true,
                    headerTitle: '',
                    headerStyle: styles.headerStyle,
                    headerLeft: () => null,
                    headerTitle: () =>  (<Header navigation={navigation} />),
                    headerRight: () =>  null,

                })}
            />
           <Stack.Screen 
                name="Verification" 
                component={VerificationScreen} 
                options={({navigation}) => ({
                    headerShown: true,
                    headerTitle: '',
                    headerStyle: styles.headerStyle,
                    headerLeft: () => null,
                    headerTitle: () =>  (<Header navigation={navigation} />),
                    headerRight: () =>  null,
                })}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.white,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})

export default LoginStack