import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';

import HomeScreen from '../../screens/dashboard/home';
import HomeHeader from '../../components/header/home';


const Stack = createStackNavigator();


const HomeStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={({navigation}) => ({
                    headerShown: true,
                    headerTitle: '',
                    headerRight: () => null,
                    headerLeft: () =>  (<HomeHeader navigation={navigation} />),
                })}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})


export default HomeStack