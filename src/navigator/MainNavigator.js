import React, { useEffect,useState } from 'react'
import { Text, View, StyleSheet, Dimensions } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import HomeStack from './home';
import LoginStack from './login';
import Tabbar from './tabbar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();

export default function MainNavigator() {


    useEffect(() => {
        getUserDetails()
    }, [])

    const session_token = useSelector(state => state.home.session_token);

    const getUserDetails = async () => {
        
        let __token__ = await AsyncStorage.getItem('__token__'); 
        let __user__ = await AsyncStorage.getItem('__user__'); 
        let wcOrg = await AsyncStorage.getItem('wcOrg');  

        console.log('===================!!!!!=================');
        console.log(" __token__  --> ", __token__);
        console.log(" __user__  --> ", __user__);
        console.log(" wcOrg ---> ", wcOrg);
        console.log('===================!!!!!=================');
        
    }
    

    return(
        <>
          <Stack.Navigator initialRouteName={session_token != null ? 'Tabbar' : 'LoginStack'} >
                <Stack.Screen 
                    name="LoginStack" 
                    component={LoginStack} 
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Tabbar" 
                    component={Tabbar} 
                    options={{
                        headerShown: false
                    }}
                />
            </Stack.Navigator>
        </>
    )
}

