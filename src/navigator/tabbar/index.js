import React,{useEffect, useState, useRef} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import { useSelector } from 'react-redux';


import { media } from '../../global/media';

import HomeScreen from '../../screens/dashboard/home';
import SearchScreen from '../../screens/dashboard/search';
import SmartHireScreen from '../../screens/dashboard/smarthire';
import ProfileScreen from '../../screens/dashboard/profile';
import JobsScreen from '../../screens/dashboard/jobs';

import SmartHireSheet from '../../components/sheet/SmartHireSheet';
import EditDetailsSheet from '../../components/sheet/EditDetailsSheet';
import HomeHeader from '../../components/header/home';


import SuccessSheet from '../../components/sheet/SuccessSheet';
import JobsHeader from '../../components/header/jobs';
import SearchHeader from '../../components/header/search';
import ProfileHeader from '../../components/header/profile';


const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {
    
    const refRBSheet = useRef();
    const editRefRBSheet = useRef();
    const successRefRBSheet = useRef();
    
    return(
        <>
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 26, width = 26 ;

                if (route.name === 'HomeScreen') {
                    iconName = !focused ? media.home : media.home_fill
                    routeName = 'Home' 
                } 
                else if (route.name === 'SearchScreen') {
                    iconName = !focused ?  media.search : media.search_fill
                    routeName = 'Search' 
                } 
                else if (route.name === 'JobsScreen') {
                    iconName = !focused ? media.jobs : media.jobs_fill
                    routeName = 'Jobs' 
                } 
                else if (route.name === 'ProfileScreen') {
                    iconName = !focused ?  media.messages : media.messages_fill
                    height = 22
                    width = 22
                    routeName = 'Candidates' 
                } 
                return(
                    <View style={{alignItems: 'center', justifyContent: 'center',}} >
                        <Image source={iconName} style={{height: height, width: width,  resizeMode: 'contain'}}/>
                         <Text style={{fontSize: 12, color: focused ? '#1F8268' : '#9E9E9E', fontWeight: '600'}} >{routeName}</Text>
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name='HomeScreen'
                component={HomeScreen}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerLeft: () => null,
                    headerStyle: styles.headerStyle,
                    headerTitle: () =>  (<HomeHeader navigation={navigation} />),
                    headerRight: () =>  null,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />
              <Tab.Screen
                name='SearchScreen'
                component={SearchScreen}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerLeft: () => null,
                    headerStyle: styles.headerStyle,
                    headerTitle: () =>  (<SearchHeader navigation={navigation} />),
                    headerRight: () =>  null,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />

            <Tab.Screen
                name='SmartHireScreen'
                component={SmartHireScreen}
                options={{
                    tabBarIcon: ({focused}) => (
                        <Image source={media.home} style={{height: 24, width: 24}} />
                    ),
                    tabBarButton: (props) => (
                        <TouchableOpacity
                            activeOpacity={0.85}
                            onPress={() => refRBSheet.current.open()}
                            style={{
                                top: -20,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <View
                                style={{
                                    height: 70,
                                    width: 70, 
                                    borderRadius: 35,
                                    borderWidth: 0.1,
                                    borderColor: '#5E6C84',
                                    backgroundColor: '#2BB793',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 6,
                                    },
                                    shadowOpacity: 0.37,
                                    shadowRadius: 7.49,

                                    elevation: 6,
                                }}
                            >
                               <Image source={media.apna_name} style={{height: 38, width: 55, marginTop: 0, resizeMode: 'contain'}} />
                            </View> 
                        </TouchableOpacity>
                    )
                }}
            />
            
            <Tab.Screen
                name='JobsScreen'
                component={JobsScreen}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerLeft: () => null,
                    headerStyle: styles.headerStyle,
                    headerTitle: () =>  (<JobsHeader navigation={navigation} />),
                    headerRight: () =>  null,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />
            <Tab.Screen
                name='ProfileScreen'
                component={ProfileScreen}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerLeft: () => null,
                    headerStyle: styles.headerStyle,
                    headerTitle: () =>  (<ProfileHeader navigation={navigation} />),
                    headerRight: () =>  null,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />
        </Tab.Navigator>
        
        <SmartHireSheet 
            refRBSheet={refRBSheet} 
            onCloseHireSheet={() => refRBSheet.current.close()} 
            onOpenHireSheet={() => refRBSheet.current.open()}  
            onCloseEditSheet={() => editRefRBSheet.current.close()} 
            onOpenEditSheet={() => editRefRBSheet.current.open()}  
            onCloseSuccessSheet={() => successRefRBSheet.current.close()} 
            onOpenSuccessSheet={() => successRefRBSheet.current.open()} 
        />
        
        <EditDetailsSheet 
            editRefRBSheet={editRefRBSheet} 
            onCloseHireSheet={() => refRBSheet.current.close()} 
            onOpenHireSheet={() => refRBSheet.current.open()}  
            onCloseEditSheet={() => editRefRBSheet.current.close()} 
            onOpenEditSheet={() => editRefRBSheet.current.open()} 
            onCloseSuccessSheet={() => successRefRBSheet.current.close()} 
            onOpenSuccessSheet={() => successRefRBSheet.current.open()} 
        />
        
        <SuccessSheet 
            successRefRBSheet={successRefRBSheet} 
            onCloseHireSheet={() => refRBSheet.current.close()} 
            onOpenHireSheet={() => refRBSheet.current.open()}  
            onCloseEditSheet={() => editRefRBSheet.current.close()} 
            onOpenEditSheet={() => editRefRBSheet.current.open()} 
            onCloseSuccessSheet={() => successRefRBSheet.current.close()} 
            onOpenSuccessSheet={() => successRefRBSheet.current.open()} 
        />

        </>
    )
}    



const styles = StyleSheet.create({
    tabbarStyle: {
        height: 70,
        paddingBottom: 5,
        alignItems:"center",
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    headerStyle: {
        //backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})
