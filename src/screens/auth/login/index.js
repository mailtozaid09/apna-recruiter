import React, {useState, useEffect} from 'react'
import { Text, View, TextInput,  StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity, ScrollView, Alert } from 'react-native'

//import auth from '@react-native-firebase/auth';


import { screenWidth } from '../../../utils/constants';
import { media } from '../../../global/media';
import { Inter } from '../../../global/fontFamily';
import { colors } from '../../../global/colors';
import LoginButton from '../../../components/button';
import { sendOtp } from '../../../global/api';

const companiesData = [
    {
        id: 1,
        icon: media.work,
        iconColor: '#fff9db',
        title: 'Post a job',
        subTitle: 'Tell us what you need in a candidate in just 3 minutes',
    },
    {
        id: 2,
        icon: media.verified_user,
        iconColor: '#ebf3fe',
        title: 'Get verified',
        subTitle: 'Our team will call to verify your employer account',
    },
    {
        id: 3,
        icon: media.how_to_reg,
        iconColor: '#eaf8f4',
        title: 'Interview & Hire',
        subTitle: 'Get relevant screened candidates and start hiring',
    },
]

const LoginScreen = ({navigation}) => {

    useEffect(() => {
        // auth().onAuthStateChanged((user) => {
        //     if(user) {
        //         console.log('====================================');
        //         console.log("user authenticated => ", user);
        //         console.log('====================================');
        //     } else {
        //         console.log('====================================');
        //         console.log("user not authenticated => ", user);
        //         console.log('====================================');
        //     }
        //   })
 
    }, [])
    
    
    const [number, setNumber] = useState('');

    const [error, setError] = useState('');

    const [buttonLoder, setButtonLoder] = useState(false);

    const sendOtpFunction = async() => {
        console.log("sendOtpFunction");
        setButtonLoder(true)

        // var phoneNumber = number

        // var phoneNumberPrefix = '+91'+ number
        // navigation.navigate('Verification', {params: {phoneNumber: phoneNumber, phoneNumberPrefix: phoneNumberPrefix}})

        if(number.length == 10){
            var phoneNumber = number

            var phoneNumberPrefix = '91'+ number

            var body = {
                "phone_number": phoneNumberPrefix,
                "retries":0,
                "hash_type":"employer",
                "source":"employer"
            }

            sendOtp(body)
            .then((resp) => {
                console.log("resp => ", resp);
                setButtonLoder(false)

                if(resp?.message == 'otp sent'){
                    navigation.navigate('Verification', {params: {phoneNumber: phoneNumber, phoneNumberPrefix: phoneNumberPrefix}})
                }else{
                    Alert.alert('ERROR!')
                    setButtonLoder(false)
                }
                
            })
            .catch((err) => {
                console.log("err => ", err);
                setButtonLoder(false)
            })

        }else{
            setButtonLoder(false)
            setError('Please enter valid mobile number')
        }
    }


    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <View style={{ width: screenWidth, padding: 20,}} >
                    <View style={{ width: '100%'}} >
                        <Text style={styles.title} >Hire from the pool of 3cr+ candidates  🚀</Text>
                    </View>

                    <View style={{alignItems: 'flex-start', width: '100%'}} >
                        <Text style={styles.inputLabel}>Mobile Number</Text>
                    </View>

                    <View style={styles.inputContainer} >
                        
                        <View style={styles.countryCode} >
                            <Text style={styles.countryCodeText} >+91</Text>
                        </View>
                        
                        <View style={styles.divider} />
                        
                        <View style={{flex: 1}} >
                            <TextInput
                                placeholder='Enter mobile number'
                                placeholderTextColor={colors.gray}
                                style={styles.inputStyle}
                                value={number}
                                maxLength={10}
                                keyboardType='number-pad'
                                onChangeText={(text) => {setNumber(text); setError('')}}
                            />
                        </View>
                        
                    </View>

                    {error && <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 6,}} >
                        <Image source={media.error} style={{height: 24, width: 24, marginRight: 6}} />
                        <Text style={{fontSize: 12, fontFamily: Inter.Regular, color: '#CC0000',}}>{error}</Text>
                    </View>}
                
                    <View style={{marginTop: 30}} >
                        <LoginButton
                            title="CONTINUE"
                            buttonLoader={buttonLoder}
                            onPress={() => {sendOtpFunction()}}
                        />
                    </View>

                <View style={{height: 1, marginVertical: 30, marginBottom: 6, width: '100%', backgroundColor: colors.light_gray }} />
                
                <View>
                    <Text style={{fontSize: 14, fontFamily: Inter.Regular, color: '#172B4D', textAlign: 'center'}} >Trusted and loved by over 2 lakhs+ companies</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly'}} >
                        <Image source={media.paytm} style={{height: 70, width: 72, resizeMode: 'contain'}} />
                        <Image source={media.byjus} style={{height: 70, width: 85, resizeMode: 'contain'}} />
                        <Image source={media.swiggy} style={{height: 65, width: 80, resizeMode: 'contain'}} />
                    </View>
                    <Text style={{fontSize: 20, marginBottom: 20, fontFamily: Inter.SemiBold, color: '#172B4D', textAlign: 'center'}} >Get started with 03 easy steps</Text>
                

                    {companiesData?.map((item, index) => (
                        <View key={index} style={{flexDirection: 'row', width: '100%', marginBottom: 20}} >
                            <View style={{height: 50, width: 50, borderRadius: 25, marginRight: 20, backgroundColor: item.iconColor, alignItems: 'center', justifyContent: 'center',}} >
                                <Image source={item.icon} style={{height: 24, width: 24}} />
                            </View>
                            <View style={{flex: 1}} >
                                <Text style={{fontSize: 16, fontFamily: Inter.SemiBold, color: '#172B4D', }}  >{item.title}</Text>
                                <Text style={{fontSize: 16, fontFamily: Inter.Regular, color: '#172B4D', }}  >{item.subTitle}</Text>
                            </View>
                        </View>
                    ))}
               
                </View>

                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        alignItems: 'center',
        width: screenWidth,
        //justifyContent: 'space-between'
    },
    loginImg: {
        height: 250,
        width: 250,
        marginTop: 100,
        margin: 20,
    },
    contentContainer: {
        padding: 20, 
        paddingTop: 0,
        alignItems: 'center', 
        
    },
    title: {
        fontSize: 22,
        fontFamily: Inter.SemiBold,
        color: colors.black,
        marginBottom: 6,
    },
    description: {
        fontSize: 16,
        fontFamily: Inter.SemiBold,
        color: colors.gray,
        marginBottom: 10,
    },
    loginButton: {
        height: 50,
        borderRadius: 8,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 0,
        marginBottom: 10,
        backgroundColor: '#1F8268',
    },
    loginButtonText: {
        fontSize: 16,
        color: colors.white,
        fontFamily: Inter.SemiBold,
    },
    divider: {
        backgroundColor: '#B3BAC5', 
        width: 1, 
        height: 30,
    },
    inputContainer: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: 10, 
        backgroundColor: colors.white, 
        borderWidth: 1, 
        borderColor: '#B3BAC5',
        borderRadius: 8, 
        width: '100%',
    },
    inputLabel: {
        fontSize: 16, 
        marginTop: 20,
        fontFamily: Inter.SemiBold,
        color: colors.black,
        textAlign: 'left',
    },
    inputStyle: {
        height: 50,
        width: '100%',
        paddingLeft: 15,
        fontSize: 18, 
        color: colors.black
    },
    countryCode: {
        height: 50, 
        width: 50, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    countryCodeText: {
        fontSize: 18, 
        color: colors.gray,
    },
})

export default LoginScreen