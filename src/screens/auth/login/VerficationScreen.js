
import React, {useState, useEffect, useRef} from 'react'
import { Text, View, TextInput,  StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity, Alert } from 'react-native'

//import auth from '@react-native-firebase/auth';

import { screenWidth } from '../../../utils/constants';

import OTPTextView from 'react-native-otp-textinput'
import { media } from '../../../global/media';

import BackArrow from 'react-native-vector-icons/Feather'
import { Inter } from '../../../global/fontFamily';
import { colors } from '../../../global/colors';
import LoginButton from '../../../components/button';
import { sendOtp, verifyOtp } from '../../../global/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { sessionToken } from '../../../store/modules/home/actions';
import { useDispatch } from 'react-redux';


var timerVal = 60

const VerificationScreen = ({navigation, route}) => {

    let otpInput = useRef(null);

    const dispatch = useDispatch()

    const [buttonLoder, setButtonLoder] = useState(false);

    const [number, setNumber] = useState(route.params.params.phoneNumber);
    const [numberPrefix, setNumberPrefix] = useState(route.params.params.phoneNumberPrefix);

    const [resendTimer, setResendTimer] = useState(10);
   
    const [otpValue, setOtpValue] = useState('');

    const [error, setError] = useState('');

    const [confirm, setConfirm] = useState(null);
    
    useEffect(() => {
        //sendOtpFunction()
        startResendTimer()
    }, [])

    const startResendTimer = () => {
        setResendTimer(timerVal)

        if(timerVal !== 0){
            timerVal = timerVal - 1

            setTimeout(() => {
                startResendTimer()
            }, 1000);
        } 
    }



    const sendOtpFunction = async () => {

        timerVal = 60

        var numPrefix = "+91" + number

        var body = {
            "phone_number": numPrefix,
            "retries":0,
            "hash_type":"employer",
            "source":"employer"
        }


        sendOtp(body)
        .then((resp) => {
            console.log("resp => ", resp);
         

            if(resp?.message == 'otp sent'){
                Alert.alert('OTP SENT!')
                //navigation.navigate('Verification', {params: {phoneNumber: phoneNumber, phoneNumberPrefix: phoneNumberPrefix}})
            }else{
                Alert.alert('ERROR!')
             
            }
            
        })
        .catch((err) => {
            console.log("err => ", err);
            Alert.alert('ERROR!', err)
        })
    }

    const verifyOtpFunction = async () => {
        setButtonLoder(true)
        if(otpValue.length == '4'){

            var numPrefix = "91" + number
            var body = {
                "phone_number": number,
                "otp": otpValue,
                "platform": "employer",
                "phone_number_prefixed": numPrefix,
                "employee_code":null
            }

            console.log(" body => ", body);

            verifyOtp(body)
            .then((resp) => {
                console.log("verify resp => ", resp);
                if(resp?.STATUS == 'SUCCESS'){
                    console.log("token -> ", resp?.tokens?.access);
                    dispatch(sessionToken(resp?.tokens?.access))
                    
                    var user_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjE4NzMzMzkiLCJleHAiOjE2ODY3NDU0MzV9.qFACHSoufUv62HNS-mRplQLlhntl5RR2w50A-HYz98w'
                    var user_object = {"organization":{"name":"Sony India Pvt Ltd","id":1373331,"external_id":null,"domains":["hotlook.com","sony.com","ap.sony.com"],"is_consultant":false,"organization_type":"KAM","business_lead_type":"Large Enterprise"},"employer_terms_accepted":false,"employer_type":"returning_employer","consultancy":null,"is_consultant":false,"company_verification_status":"pending","lead_type":"smb","allow_adding_ats_funnels":false,"allow_csv_download":false,"tecc_allow_csv_download":false,"id":1873339,"uuid":"h33WjKm69Q","phone_number":"8050617417","is_internal_user":true,"full_name":"database","email":"","is_staff":false,"is_ats_enabled":true,"support_requested":null}
                    
                    AsyncStorage.setItem('__token__', user_token); 
                    AsyncStorage.setItem('__user__', JSON.stringify(user_object)); 

                    AsyncStorage.setItem('wcOrg', '1373759');  
                    
                    navigation.navigate('Tabbar')
                    setButtonLoder(false)
                }else{
                    setButtonLoder(false)
                    Alert.alert("Error! ", resp?.msg)
                }
            })
            .catch((err) => {
                console.log("err => ", err);
                Alert.alert("Error! ", err)
                setButtonLoder(false)
            })

            
        }else{
            setButtonLoder(false)
            setError('Pleas enter the correct OTP code')
        }
    }

   



    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.contentContainer} >
                <TouchableOpacity 
                    onPress={() => navigation.goBack()}
                    style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginBottom: 20}} >
                    <BackArrow name="arrow-left" size={24} color={colors.black} style={{marginRight: 10}} />
                    <Text style={{fontSize: 16, fontFamily: Inter.SemiBold, fontWeight: '700', color: colors.black}} >Back</Text>
                </TouchableOpacity>
                <View style={{ width: '100%'}} >
                    <Text style={styles.title} >Verify your mobile number</Text>
                    <Text style={styles.description} >A one time password sent on your mobile number <Text style={styles.numberText} >{number}</Text> </Text>
                </View>
               
               <View style={{width: '100%'}}  >

                    <Text style={{marginTop: 10, marginLeft: 2, fontSize: 14, color: colors.black, fontWeight: '700'}} >Enter the OTP</Text>
                    <OTPTextView
                        handleTextChange={e => {setOtpValue(e); setError('')}}
                        containerStyle={styles.textInputContainer}
                        textInputStyle={styles.roundedTextInput}
                        inputCount={4}
                        inputCellLength={1}
                        tintColor='#1f8269'
                    />
                        
                    {error && <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 6,}} >
                        <Image source={media.error} style={{height: 24, width: 24, marginRight: 6}} />
                        <Text style={{fontSize: 12, fontFamily: Inter.Regular, color: '#CC0000',}}>{error}</Text>
                    </View>}
                    
                    
                    <View style={{marginTop: 30}} >
                        <LoginButton
                            title="VERIFY OTP"
                            buttonLoader={buttonLoder}
                            onPress={() => {verifyOtpFunction()}}
                        />
                    </View>

                    <View>
                        {resendTimer != 0
                            ?
                                <Text style={styles.resendTimer} >Didn’t receive OTP? Resend in <Text style={{fontFamily: Inter.SemiBold}} >{resendTimer} secs</Text></Text>
                            :
                                <TouchableOpacity
                                    onPress={() => {sendOtpFunction(); startResendTimer();}}    
                                >
                                    <Text style={styles.resendTimerText} >RESEND OTP</Text>
                                </TouchableOpacity>
                                }
                    </View>
               </View>
            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        alignItems: 'center',
        padding: 20,
        paddingTop: 20,
        justifyContent: 'space-between'
    },
    loginImg: {
        height: 250,
        width: 250,
        marginTop: 100,
        margin: 20,
    },
    contentContainer: {
        padding: 20, 
        paddingTop: 0,
        alignItems: 'center', 
        width: screenWidth
    },
    numberText: {
        fontFamily: Inter.SemiBold,
        color: colors.black,
    },
    title: {
        fontSize: 22,
        fontFamily: Inter.SemiBold,
        color: colors.black,
        marginBottom: 10,
    },
    description: {
        fontSize: 16,
        color: colors.gray,
        fontFamily: Inter.SemiBold,
        marginBottom: 10,
    },
    loginButton: {
        height: 50,
        borderRadius: 8,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: '#1F8268',
    },
    loginButtonText: {
        fontSize: 16,
        fontFamily: Inter.SemiBold,
        color: colors.white,
    },
    inputStyle: {
        height: 50,
        width: '100%',
        paddingLeft: 10,
        fontSize: 18, 
        color: colors.black
    },
    resendTimer: {
        fontSize: 14,
        color: '#5E6C84',
        fontFamily: Inter.Regular,
        marginTop: 10,
    },
    resendTimerText: {
        fontSize: 16,
        textAlign: 'center',
        color: colors.gray,
        fontFamily: Inter.SemiBold,
        marginTop: 10,
        textDecorationLine: 'underline',
    },
    textInputContainer: {
        
        marginTop: 0,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
      },
      roundedTextInput: {
        borderRadius: 8,
        borderWidth: 1,
        borderBottomWidth: 1,
        height: 44,
        width: 44,
        fontSize: 16,
        fontFamily: Inter.Medium
      },
})

export default VerificationScreen