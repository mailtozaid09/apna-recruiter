
import React from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity, Linking } from 'react-native'
import WebView from 'react-native-webview';

var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjE4NzMzMzkiLCJleHAiOjE2ODY3NDU0MzV9.qFACHSoufUv62HNS-mRplQLlhntl5RR2w50A-HYz98w'
var user = {"organization":{"name":"Sony India Pvt Ltd","id":1373331,"external_id":null,"domains":["hotlook.com","sony.com","ap.sony.com"],"is_consultant":false,"organization_type":"KAM","business_lead_type":"Large Enterprise"},"employer_terms_accepted":false,"employer_type":"returning_employer","consultancy":null,"is_consultant":false,"company_verification_status":"pending","lead_type":"smb","allow_adding_ats_funnels":false,"allow_csv_download":false,"tecc_allow_csv_download":false,"id":1873339,"uuid":"h33WjKm69Q","phone_number":"8050617417","is_internal_user":true,"full_name":"database","email":"","is_staff":false,"is_ats_enabled":true,"support_requested":null}


const SearchScreen = () => {

    const INJECTED_JAVASCRIPT = `(function() {
 
        window.localStorage.setItem("__token__", 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI2NzM4MTIiLCJleHAiOjE2ODcwODA3ODZ9.BXsVijKgO6ne8tJ7tBHH0PeSHfSAtTwFQapBI7_IUzk');
       
       
      
    })();`;

    // window.localStorage.setItem("__user__", JSON.stringify({"organization":{"name":"Sony India Pvt Ltd","id":1373331,"external_id":null,"domains":["hotlook.com","sony.com","ap.sony.com"],"is_consultant":false,"organization_type":"KAM","business_lead_type":"Large Enterprise"},"employer_terms_accepted":false,"employer_type":"returning_employer","consultancy":null,"is_consultant":false,"company_verification_status":"pending","lead_type":"smb","allow_adding_ats_funnels":false,"allow_csv_download":false,"tecc_allow_csv_download":false,"id":1873339,"uuid":"h33WjKm69Q","phone_number":"8050617417","is_internal_user":true,"full_name":"database","email":"","is_staff":false,"is_ats_enabled":true,"support_requested":null})  );
    const onMessage = (payload) => {
        console.log('payload', payload);
    };

    return (
        <View style={{flex: 1}}>
            <WebView
                source={{ uri: 'https://employer-test.apna.co/search',}}
                injectedJavaScript={INJECTED_JAVASCRIPT}
                onMessage={onMessage}
            />
    </View>
    )
}

export default SearchScreen
