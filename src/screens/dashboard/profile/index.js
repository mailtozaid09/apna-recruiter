import React, {useState, useEffect} from 'react'
import {StyleSheet, Text, View, Image} from 'react-native';
import SwipeCards from 'react-native-swipe-cards';
import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { Inter } from '../../../global/fontFamily';
import { media } from '../../../global/media';
import { profileCards } from '../../../global/sampleData';
import ProfileCard from '../../../components/custom/ProfileCard';

import DownArrow from 'react-native-vector-icons/Entypo'
  
const ProfileScreen = () => {

    const [cards, setCards] = useState(profileCards);
    const [outOfCards, setOutOfCards] = useState(false);

     
    const handleYup = (card) => {
        console.log("yup")
    }
 
    const handleNope = (card) => {
        console.log("nope")
    }
 
    const cardRemoved = (index) => {
    console.log(`The index is ${index}`);
 
    let CARD_REFRESH_LIMIT = 3
 
        if (cards.length - index <= CARD_REFRESH_LIMIT + 1) {
            console.log(`There are only ${cards.length - index - 1} cards left.`);
        
            if (!outOfCards) {
                console.log(`Adding ${profileCards.length} more cards`)
                
                setCards(cards.concat(profileCards))
                setOutOfCards(true)
            }
        }
    }



    const NoMoreCards = () => {
        return(
            <View style={styles.noMoreCards}>
                <Text>No more cards</Text>
            </View>
        )
    }

    return (
        <View style={styles.container} >
            
            
            
            <View style={{backgroundColor: 'white', padding: 20, paddingBottom: 0}} >
                <Text style={styles.profiles} ><Text style={{fontSize: 18, fontFamily: Inter.Bold }} >50</Text> profiles recommended by <Text style={{fontFamily: Inter.Bold, fontSize: 22, color:'#4D3951'}} >apna</Text> AI </Text>
                <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 20}} >
                    <Text style={{fontSize: 16, fontFamily: Inter.Regular, color: '#5E6C84', marginRight: 10}} >Graphic Design Intern</Text>
                    <DownArrow name="chevron-small-down" size={28} color="#5E6C84" />
                </View>
            </View>

            <SwipeCards
                cards={cards}
                loop={false}
        
                renderCard={(cardData) => <ProfileCard {...cardData} />}
                renderNoMoreCards={() => <NoMoreCards />}
                showYup={true}
                showNope={true}
                
                yupStyle={styles.yupStyle}
                yupTextStyle={styles.yupTextStyle}
                yupText="Saved"

                nopeStyle={styles.nopeStyle}
                nopeTextStyle={styles.nopeTextStyle}
                nopeText="Pass"


                handleYup={() => handleYup()}
                handleNope={() => handleNope()}
                cardRemoved={() => cardRemoved()}
            />
        </View>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1, 
        //padding: 20,
        //alignItems: 'center',
        width: screenWidth,
        backgroundColor: '#F2F2F0',
    },
    profiles: {
        fontSize: 16,
        fontFamily: Inter.Medium,
        color: '#172B4D',
        marginBottom: 10
    },
    card: {
        width: screenWidth-60,
        alignItems: 'center',
        borderRadius: 10,
        // overflow: 'hidden',
        borderColor: '#1F8268',
        backgroundColor: 'white',
        borderWidth: 1,
        elevation: 1,
    },
    profileTop: {
       paddingVertical: 20,
        width: '100%', 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E9F8F4',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    profileBottom: {
        alignItems: 'flex-start', 
        width: '100%',
        padding: 20,
        paddingTop: 10,
    },
    viewProfile: {
        padding: 10,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    viewProfileText: {
        fontSize: 16,
        fontFamily: Inter.Medium,
        color: '#1F8268',
    },
    title: {
        fontSize: 20,
        fontFamily: Inter.SemiBold,
        color: '#172B4D',
        marginBottom: 4,
        marginTop: 10,
    },
    subTitle: {
        fontSize: 16,
        fontFamily: Inter.Medium,
        textAlign: 'center',
        color: '#172B4D',
        marginBottom: 4
    },
    description: {
        fontSize: 14,
        fontFamily: Inter.Regular,
        color: '#172B4D',
        marginBottom: 4,
    },
    thumbnail: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 2,
    },
    text: {
      fontSize: 20,
    },
    noMoreCards: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    yupStyle: {
        borderWidth: 1,
        borderColor: '#1F8268',
        padding: 14,
        paddingVertical: 8,
        borderRadius: 8,
        marginRight: 20
    },
    yupTextStyle: {
        fontSize: 14,
        fontFamily: Inter.Medium,
        color: '#1F8268',
    },
    nopeStyle: {
        borderWidth: 1,
        borderColor: '#CC0000',
        padding: 14,
        paddingVertical: 8,
        borderRadius: 8,
        marginLeft: 20
    },
    nopeTextStyle: {
        fontSize: 14,
        fontFamily: Inter.Medium,
        color: '#CC0000',
    },
  })

export default ProfileScreen
