import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity } from 'react-native'
import { colors } from '../../../utils/colors'


const SmartHireScreen = ({navigation}) => {

    const [userDetails, setUserDetails] = useState();

    useEffect(() => {

    
    }, [])
    


    return (
        <SafeAreaView style={styles.container} >
                <Text style={styles.heading} >SmartHire Screen</Text>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
    },
    heading: {
        fontSize: 22,
        color: colors.black,
    }
})

export default SmartHireScreen