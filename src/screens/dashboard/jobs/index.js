import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity, Linking, ActivityIndicator, Alert } from 'react-native'


import JobList from '../../../components/custom/JobList';
import { colors } from '../../../global/colors';
import { jobsData } from '../../../global/sampleData';
import JobFilter from '../../../components/custom/JobFilter';
import { getJobs } from '../../../global/api';
import { Inter } from '../../../global/fontFamily';
import { useSelector } from 'react-redux';



const JobsScreen = ({navigation}) => {

    const session_token = useSelector(state => state.home.session_token);

    const [jobsDataArr, setJobsDataArr] = useState([]);

    const [loader, setLoader] = useState(true);

    useEffect(() => {
            
        getAllJobs()
                
    }, [])
    
    const getAllJobs = () => {
        getJobs(session_token)
        .then((resp) => {
            console.log('resp => ', resp);
            setJobsDataArr(resp?.results)
            setLoader(false)
        })
        .catch((err) => {
            console.log('err => ', err);
            Alert.alert("ERROR! ", err)
            setLoader(false)
        })
    }


    const NoJobsCard = () => {
        return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                <Text style={styles.title} >Ooopss!!!</Text>
                <Text style={styles.subtitle} >No Jobs Found</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            
            <View style={{width: '100%', flex: 1}} >
                {loader
                    ?
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                        <ActivityIndicator size="large" color="#1F8268" />
                    </View>
                    :
                    <>
                    {jobsDataArr?.length != 0
                    ?
                        <View style={{width: '100%', flex: 1}} >

                            <View style={{flexDirection: 'row', paddingTop: 20, paddingBottom: 15, backgroundColor: colors.white, paddingHorizontal: 20, alignItems: 'center', justifyContent: 'space-between'}} >
                                <Text style={styles.alljobs} >All Jobs <Text style={{fontSize: 14, color: '#5E6C84'}} >({jobsDataArr?.length})</Text> </Text>

                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={styles.postButton}
                                >
                                    <Text style={styles.postButtonText} >Post a new job</Text>
                                </TouchableOpacity>
                            </View>

                            <JobFilter />
                    
                            <JobList 
                                data={jobsDataArr}
                            />
                        </View>
                    :
                        <NoJobsCard />
                    }
                    </>
                }
            </View>

            

            
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //paddingTop: 20,
        //padding: 20,
        alignItems: 'center',
        //justifyContent: 'center',
        backgroundColor: '#F2F2F0',
    },
    heading: {
        fontSize: 22,
        color: colors.black,
    },
    title: {
        fontSize: 30,
        fontFamily: Inter.Bold,
        color: '#172B4D',
    },
    subtitle: {
        fontSize: 22,
        marginTop: 10,
        fontFamily: Inter.Medium,
        color: '#172B4D',
    },
    alljobs: {
        fontSize: 20,
        fontFamily: Inter.SemiBold,
        color: '#172B4D',
    },
    postButton: {
        backgroundColor: '#1F8268',
        padding: 14,
        paddingVertical: 8,
        borderRadius: 8,
    },
    postButtonText:{
        fontSize: 14,
        color: colors.white,
        fontFamily: Inter.Regular,
    }
})

export default JobsScreen
