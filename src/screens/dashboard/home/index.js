import React, {useState, useEffect, useRef} from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, TouchableOpacity, BackHandler, Alert, FlatList, ScrollView } from 'react-native'
import { colors } from '../../../utils/colors'
import { useDispatch } from 'react-redux'

import { Inter } from '../../../global/fontFamily'
import { media } from '../../../global/media'


const HomeScreen = ({navigation}) => {

  const jobOptions = [
    {
        id: 1,
        title: `Post a \n Job`,
        color: '#E9F8F4',
        image: media.post_job,
    },
    {
        id: 2,
        title: 'Search Database',
        color: '#E3EEFF',
        image: media.search_db,
    },
    {
        id: 3,
        title: 'Scan Phone No.',
        color: '#FEF3D9',
        image: media.scan_contact,
    },
    {
        id: 4,
        title: `Saved\nCandidates`,
        color: '#FAECEE',
        image: media.saved_candidates,
    },
  ]

    const upgradeCard = [
        {
            id: 1,
            title: '5 profiles unlocked for free',
            subtitle: 'apna AI has recommended 5 profiles for graphic designer job',
            color: '#E9F8F4',
            type: 'job',
            image: media.mask_img_1,
            mask: media.mask_1,
        },
        {
            id: 2,
            title: 'New applications received',
            subtitle: 'Accountant job has received 10 new appliations',
            color: '#FFF7E5',
            type: 'application',
            image: media.mask_img_2,
            mask: media.mask_2,
        },
    ]

    const refRBSheet = useRef();

    const dispatch = useDispatch()

    const [userDetails, setUserDetails] = useState();

    useEffect(() => {
        const backAction = () => {
          Alert.alert("Hold on!", "Are you sure you want to exit App?", [
            {
              text: "Cancel",
              onPress: () => null,
              style: "cancel"
            },
            { text: "YES", onPress: () => BackHandler.exitApp() }
          ]);
          return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
      }, []);


    return (
        <SafeAreaView style={styles.container} >
                <ScrollView showsVerticalScrollIndicator={false} >
                <View style={{marginTop: 10}} >

                    <FlatList
                        data={jobOptions}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{width: '100%', height: 120, flexDirection: 'row', justifyContent: 'space-between'}}
                        renderItem={({item, index}) => (
                            <TouchableOpacity key={index} style={{width: 78,  alignItems: 'center'}} >
                                <View style={{height: 60, width: 60, alignItems: 'center', justifyContent: 'center', borderRadius: 6, backgroundColor: item.color, }} >
                                    <Image source={item.image} style={{height: 36, width: 36,}} />
                                </View>     

                                <Text style={{fontSize: 14, marginTop: 4, fontFamily: Inter.SemiBold, color: '#172B4D', textAlign: 'center', }} >{item.title}</Text>
                            </TouchableOpacity>
                        )}
                    />


                    <View style={{alignItems: 'center', marginVertical: 30}} >
                        <View style={{height: 1, width: 70, backgroundColor: '#25323F50'}} />
                    </View>
                
                    <Text style={styles.heading} >Hey <Image source={media.hey} style={{height: 24, width: 24, }} /> Rahul!</Text>
                    <Text style={styles.subHeading} >We have updates for you</Text>
                
                    <View>
                        <FlatList
                            data={upgradeCard}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{width: '100%', }}
                            keyExtractor={item => item.id}
                            renderItem={({item, index}) => (
                                <View key={index} style={[styles.upgradeCard, {backgroundColor: item.color}]} >
                                    <View>
                                        <Image source={item.image} style={{height: 80, width: 80, marginRight: 15 }} />
                                    </View>

                                    <View style={{position: 'absolute', bottom: -5, right: 0}} >
                                        <Image source={item.mask} style={{height: 150, width: 150, resizeMode: 'contain'}} />
                                    </View>

                                    <View style={{ alignItems: 'flex-start', flex: 1}} >
                                        <View style={{ flex: 1}} >
                                            <Text style={{fontSize: 16, fontFamily: Inter.Bold, color: '#283D68'}} >{item.title}</Text>
                                            <Text style={{fontSize: 14, marginTop: 5, marginBottom: 5, fontFamily: Inter.Regular, color: '#5E6C84'}} >{item.subtitle}</Text>
                                        </View>
                                    
                                        <TouchableOpacity style={styles.jobButton} > 
                                            <Text style={{fontSize: 12, fontFamily: Inter.SemiBold, color: '#fff'}} >{item.type == 'job' ? 'View Job' : 'View Applications'}</Text>
                                        </TouchableOpacity>
                                    </View>


                                   
                                </View>
                            )}
                        />
                    </View>
                
                </View>
                </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: colors.white,
    },
    heading: {
        fontSize: 20,
        //marginTop: 10,
        fontFamily: Inter.Bold,
        marginBottom: 4,
        color: colors.black,
    },
    subHeading: {
        fontSize: 16,
        color: '#5E6C84',
        marginBottom: 20,
        fontFamily: Inter.Regular,
    },
    upgradeCard: {
        //borderWidth: 1,
        marginBottom: 15,
        borderColor: '#ced4da',
        borderRadius: 8,
        padding: 10,
        paddingVertical: 20,
        paddingLeft: 0,
        flexDirection: 'row',
        alignItems: 'center',
    },
    jobButton: {
        padding: 4,
        borderRadius: 4,
        paddingHorizontal: 12,
        marginTop: 4,
        backgroundColor: '#1F8268'
    }
})

export default HomeScreen



