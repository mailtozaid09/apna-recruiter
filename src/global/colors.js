export const colors = {
    accent: '#211f1f',

    background: '#f4f2f7',

    dark: '#121212',
    red: '#e50914',
    light: '#fff',
    grey: '#8c8c8c',

    primary: '#B1060F',
    primary2: '#D22F26',
    primary3: '#DF7375',

    black: '#172B4D',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    
   
    white: '#ffffff',
    
    dark_gray: '#737373',
    dark_gray2: '#323232',
    dark_gray3: '#191919',

    light_gray: '#e6e6e6',
    light_gray2: '#cbcbcb',
    light_gray3: '#b2b2b2',
}