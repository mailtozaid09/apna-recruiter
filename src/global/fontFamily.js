export const Inter = {
    Bold: 'Inter-Bold',
    Light: 'Inter-Light',
    Medium: 'Inter-Medium',
    Regular: 'Inter-Regular',
    SemiBold: 'Inter-SemiBold',
};
  

export const fontSize = {
  Heading: 30,
  SubHeading: 24,
  Title: 20,
  SubTitle: 16,
  Body: 14,
}