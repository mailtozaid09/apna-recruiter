const baseUrl = 'https://backend-stg.apna.co'


// 'https://backend-stg.apna.co/api/userprofile/v1/otp/'

// 'https://backend-stg.apna.co/apna-auth/v2/employer/login/'

// 'https://api.staging.infra.apna.co/cerebro/api/v1/white-collar-search/'

// 'https://backend-stg.apna.co/api/employer/v7/jobs/?page_size=5'

export function sendOtp(body) {
    return(
        fetch(
            `${baseUrl}/api/userprofile/v1/otp/`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}

export function verifyOtp(body) {
    return(
        fetch(
            `${baseUrl}/apna-auth/v2/employer/login/`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}

export function getJobs(token) {

   // var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjMxNzUzNDkiLCJleHAiOjE2ODY5MTEzNTB9.22zMUDsjD0yN8svwuLNvjarhXKwQPSVFYl989XB9HzA'
    return(
        fetch(
            `${baseUrl}/api/employer/v7/jobs/?page_size=5`,
            {
              method: "GET",
              headers: {
                'Authorization': `Token ${'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI2NzM4MTIiLCJleHAiOjE2ODcwODA3ODZ9.BXsVijKgO6ne8tJ7tBHH0PeSHfSAtTwFQapBI7_IUzk'}`,
              }
            },
            
        )
        .then(res => res.json())
    );
}

export function getCandidates(body) {
    return(
        fetch(
            `https://api.staging.infra.apna.co/cerebro/api/v1/white-collar-search/`,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}