export const media = {

    


    apnaRecruiter: require('../assets/apnaRecruiter.png'),
    apnaHire: require('../assets/apnaHire.png'),

    ticks: require('../assets/ticks.png'),
    left_rect: require('../assets/left_rect.png'),
    right_rect: require('../assets/right_rect.png'),
    
    companies: require('../assets/companies.png'),

    menu: require('../assets/menu.png'),

    sort: require('../assets/sort.png'),

    post_job: require('../assets/post_job.png'),
    post_job_green: require('../assets/post_job_green.png'),
    saved_candidates: require('../assets/saved_candidates.png'),
    scan_contact: require('../assets/scan_contact.png'),
    search_db: require('../assets/search_db.png'),
    mask_img_1: require('../assets/mask_img_1.png'),
    mask_img_2: require('../assets/mask_img_2.png'),
    mask_1: require('../assets/mask_1.png'),
    mask_2: require('../assets/mask_2.png'),
    search_icon: require('../assets/search_icon.png'),

    dummy_user: require('../assets/dummy_user.png'),
    
    job: require('../assets/job.png'),

    job_icon: require('../assets/job_icon.png'),


    salary: require('../assets/salary.png'),
    
    apna_name: require('../assets/apna_name.png'),
    microphone: require('../assets/microphone.png'),

    byjus: require('../assets/byjus.png'),
    paytm: require('../assets/paytm.png'),
    swiggy: require('../assets/swiggy.png'),
    
    work: require('../assets/work.png'),
    how_to_reg: require('../assets/how_to_reg.png'),
    verified_user: require('../assets/verified_user.png'),
   
    error: require('../assets/error.png'),
   


    hey: require('../assets/hey.png'),
    
    send: require('../assets/send.png'),

    apna: require('../assets/apna.png'),


    plus: require('../assets/tabicon/plus.png'),
    
    home: require('../assets/tabicon/home.png'),
    home_fill: require('../assets/tabicon/home_fill.png'),

    search: require('../assets/tabicon/search.png'),
    search_fill: require('../assets/tabicon/search_fill.png'),

    jobs: require('../assets/tabicon/jobs.png'),
    jobs_fill: require('../assets/tabicon/jobs_fill.png'),

    messages: require('../assets/tabicon/messages.png'),
    messages_fill: require('../assets/tabicon/messages_fill.png'),

}
