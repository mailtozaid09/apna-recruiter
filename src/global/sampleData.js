import { colors } from "./colors";
import { media } from "./media";

export const jobsData = [
    {
        "job_title": "Software Engineer",
        "company": "ABC Technologies",
        "location": "Bangalore, Karnataka",
        "salary": "10,00,000 - 15,00,000 INR",
        "requirements": ["Bachelor's degree in Computer Science", "2+ years of experience in software development"],
        "description": "ABC Technologies is seeking a skilled Software Engineer to join our team. The ideal candidate should have a strong background in software development and be proficient in programming languages like Java or Python.",
        "status": "Approved",
        "date_posted": "2023-02-10"
      },
      {
        "job_title": "Marketing Manager",
        "company": "XYZ Corporation",
        "location": "Mumbai, Maharashtra",
        "salary": "12,00,000 - 18,00,000 INR",
        "requirements": ["Bachelor's degree in Marketing or related field", "5+ years of experience in marketing"],
        "description": "XYZ Corporation is looking for an experienced Marketing Manager to develop and implement marketing strategies. The candidate should have a proven track record in creating successful marketing campaigns.",
        "status": "Approved",
        "date_posted": "2023-02-12"
      },
      {
        "job_title": "Graphic Designer",
        "company": "Design Solutions",
        "location": "Delhi, Delhi",
        "salary": "6,00,000 - 8,00,000 INR",
        "requirements": ["Bachelor's degree in Graphic Design or related field", "Proficiency in Adobe Creative Suite"],
        "description": "Design Solutions is hiring a creative Graphic Designer to produce engaging visual materials. The candidate should have a strong portfolio showcasing their design skills and the ability to work on multiple projects simultaneously.",
        "status": "Not Approved",
        "date_posted": "2023-02-15"
      },
      {
        "job_title": "Financial Analyst",
        "company": "Finance Group",
        "location": "Chennai, Tamil Nadu",
        "salary": "8,00,000 - 10,00,000 INR",
        "requirements": ["Bachelor's degree in Finance or related field", "2+ years of experience in financial analysis"],
        "description": "Finance Group is seeking a detail-oriented Financial Analyst to provide accurate financial information. The candidate should have strong analytical skills and be proficient in financial modeling and forecasting.",
        "status": "Approved",
        "date_posted": "2023-02-18"
      },
      {
        "job_title": "Human Resources Manager",
        "company": "People Solutions",
        "location": "Hyderabad, Telangana",
        "salary": "15,00,000 - 20,00,000 INR",
        "requirements": ["Bachelor's degree in Human Resources or related field", "5+ years of experience in HR management"],
        "description": "People Solutions is looking for an experienced HR Manager to oversee all aspects of human resources. The candidate should have a comprehensive understanding of HR policies and excellent leadership skills.",
        "status": "Not Approved",
        "date_posted": "2023-02-20"
      },
      {
        "job_title": "Sales Executive",
        "company": "Salesforce India",
        "location": "Pune, Maharashtra",
        "salary": "6,00,000 - 10,00,000 INR",
        "requirements": ["Bachelor's degree in Business or related field", "1+ years of experience in sales"],
        "description": "Salesforce India is hiring a dynamic Sales Executive to drive sales and expand customer base. The candidate should have excellent communication and negotiation skills.",
        "status": "Not Approved",
        "date_posted": "2023-02-22"
      },
      {
        "job_title": "Content Writer",
        "company": "Content Solutions",
        "location": "Kolkata, West Bengal",
        "salary": "4,00,000 - 6,00,000 INR",
        "requirements": ["Bachelor's degree in English, Journalism, or related field", "Excellent writing and editing skills"],
        "description": "Content Solutions is seeking a talented Content Writer to create compelling and engaging content. The candidate should have a passion for writing and a strong command of the English language.",
        "status": "Approved",
        "date_posted": "2023-02-25"
      },
      {
        "job_title": "Data Analyst",
        "company": "Analytics Inc.",
        "location": "Bangalore, Karnataka",
        "salary": "7,00,000 - 9,00,000 INR",
        "requirements": ["Bachelor's degree in Statistics, Mathematics, or related field", "Proficiency in data analysis tools like SQL, Excel, and Python"],
        "description": "Analytics Inc. is looking for a skilled Data Analyst to analyze and interpret complex data sets. The candidate should have strong analytical and problem-solving abilities.",
        "status": "Approved",
        "date_posted": "2023-02-28"
      },
      {
        "job_title": "UX Designer",
        "company": "User Experience Solutions",
        "location": "Mumbai, Maharashtra",
        "salary": "8,00,000 - 12,00,000 INR",
        "requirements": ["Bachelor's degree in Design or related field", "Experience in user-centered design and prototyping tools"],
        "description": "User Experience Solutions is seeking a talented UX Designer to create intuitive and user-friendly interfaces. The candidate should have a strong portfolio demonstrating their design skills.",
        "status": "Not Approved",
        "date_posted": "2023-03-02"
      },
      {
        "job_title": "Business Development Manager",
        "company": "Growth Strategies",
        "location": "Delhi, Delhi",
        "salary": "12,00,000 - 16,00,000 INR",
        "requirements": ["Bachelor's degree in Business Administration or related field", "Proven track record in business development"],
        "description": "Growth Strategies is looking for a motivated Business Development Manager to identify new business opportunities. The candidate should have excellent negotiation and communication skills.",
        "status": "Approved",
        "date_posted": "2023-03-05"
      },
      {
        "job_title": "Accountant",
        "company": "Financial Solutions Ltd.",
        "location": "Chennai, Tamil Nadu",
        "salary": "5,00,000 - 7,00,000 INR",
        "requirements": ["Bachelor's degree in Accounting or related field", "Knowledge of accounting principles and financial regulations"],
        "description": "Financial Solutions Ltd. is hiring an Accountant to manage financial transactions and prepare financial statements. The candidate should have strong mathematical and analytical skills.",
        "status": "Approved",
        "date_posted": "2023-03-08"
      },
      {
        "job_title": "IT Project Manager",
        "company": "Tech Solutions",
        "location": "Hyderabad, Telangana",
        "salary": "10,00,000 - 14,00,000 INR",
        "requirements": ["Bachelor's degree in Computer Science or related field", "5+ years of experience in project management"],
        "description": "Tech Solutions is seeking an experienced IT Project Manager to lead and deliver technology projects. The candidate should have strong organizational and leadership skills.",
        "status": "Approved",
        "date_posted": "2023-03-10"
      },
      {
        "job_title": "Customer Service Representative",
        "company": "Service Solutions",
        "location": "Pune, Maharashtra",
        "salary": "3,00,000 - 4,00,000 INR",
        "requirements": ["High school diploma or equivalent", "Excellent communication and problem-solving skills"],
        "description": "Service Solutions is looking for a friendly and customer-focused representative to assist customers with their inquiries. The candidate should have a positive attitude and the ability to work in a fast-paced environment.",
        "status": "Not Approved",
        "date_posted": "2023-03-12"
      },
      {
        "job_title": "Web Developer",
        "company": "Web Solutions",
        "location": "Bangalore, Karnataka",
        "salary": "6,00,000 - 8,00,000 INR",
        "requirements": ["Bachelor's degree in Computer Science or related field", "Experience in web development using HTML, CSS, and JavaScript"],
        "description": "Web Solutions is hiring a talented Web Developer to create and maintain visually appealing websites. The candidate should have a strong portfolio showcasing their web development projects.",
        "status": "Approved",
        "date_posted": "2023-03-15"
      },
      {
        "job_title": "Digital Marketing Specialist",
        "company": "Digital Solutions",
        "location": "Mumbai, Maharashtra",
        "salary": "8,00,000 - 10,00,000 INR",
        "requirements": ["Bachelor's degree in Marketing or related field", "Experience in digital marketing strategies and tools"],
        "description": "Digital Solutions is seeking a skilled Digital Marketing Specialist to develop and implement online marketing campaigns. The candidate should have a strong understanding of SEO, SEM, and social media marketing.",
        "status": "Not Approved",
        "date_posted": "2023-03-18"
      },
      {
        "job_title": "Software Quality Assurance Engineer",
        "company": "Quality Assurance Solutions",
        "location": "Delhi, Delhi",
        "salary": "7,00,000 - 9,00,000 INR",
        "requirements": ["Bachelor's degree in Computer Science or related field", "Experience in software testing and quality assurance"],
        "description": "Quality Assurance Solutions is hiring a meticulous Software QA Engineer to ensure the quality of software products. The candidate should have strong attention to detail and knowledge of testing methodologies.",
        "status": "Approved",
        "date_posted": "2023-03-20"
      },
]


export const profileCards = [
  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },


  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },


  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

  {
    "name": "Jhon Karay",
    "location": "Bangalore, Karnataka",
    "title": "User Experience and Product at Accenture",
    "workTitle_1": "Senior User Experience Designer",
    "workSubtitle_1": "Hubble Connected - Full-time",
    "workDuration_1": "Aug 2021 - Oct 2022 · 1 yr 3 mos",

    "workTitle_2": "Product Designer",
    "workSubtitle_2": "Esper - Full-time",
    "workDuration_2": "Mar 2020 - Jul 2021 · 1 yr 5 mos",

    "workTitle_3": "User Experience Designer",
    "workSubtitle_3": "Playo - Full-time",
    "workDuration_3": "Dec 2017 - Oct 2019 · 1 yr 11 mos",
  },

]