import React, {useEffect} from 'react'
import { Text, View, StyleSheet, Dimensions, Image, SafeAreaView, StatusBar, LogBox } from 'react-native'

import { NavigationContainer } from '@react-navigation/native'

import { colors } from './src/utils/colors'
import Navigator from './src/navigator'

import { Provider, useSelector, } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './src/store';
import SplashScreen from 'react-native-splash-screen'

const App = () => {

    LogBox.ignoreAllLogs(true)

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide(); 
        }, 2000);
      }, []);


    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <NavigationContainer>
                    <StatusBar backgroundColor={colors.black} />  
                    <Navigator />
                </NavigationContainer>
            </PersistGate>
        </Provider>
    )
}

export default App